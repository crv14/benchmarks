=============
NestedCommands
=============

See README.txt for an overview.

--------------------------
a.i. Formal Representation
--------------------------

QEA:
forall c_1, forall c_2

next(1)[
  command(c_1) --> 2
  command(c_2) --> 1
  succeed(c_2) --> 1
]
next(2)[
  succeed(c_1) --> 1
  command(c_2) --> 3
]
next(3)[
  succeed(c_2) --> 2
]

accepts(1,2,3)

Propositional part as a CFG:

S -> epsilon | command S success

Clarification: There was a mismatch between event names in
the specification and log file.

--------------------------
a.ii. Informal Explanation
--------------------------

If command with identifier B starts after command with identifier A has
started then command B must succeed before command A succeeds. A command
can only be started and can only succeed once.

Clarification: The fact that a command can only be started and can only
succeed once is an assumption, not a requirement.

Clarification: This property describes the relationship between different
commands, and not the correct behaviour for a single command - this is
assumed. Therefore, we assume that for each command we have command then
succeed exactly once.

Examples of correct traces:
command(1).command(2).success(2).success(1)
command(1).success(1).command(2).success(2)
command(1).command(2).command(3).success(3).success(2).success(1)

Examples of incorrect traces:
command(1).command(1)
command(1).command(2).success(1).success(2)

--------------------------
a.iii. Verdict
--------------------------

The trace supplied violates this property i.e. evaluates to False.

--------------------------
b. Instrumentation information
--------------------------

Note that this specification requires single concrete events to be related to 
multiple abstract events i.e. the concrete event
command,arg0=1991864949
will result in two abstract events, one for c_1=1991864949 and one for c_2=1991864949

abstract event: command(c_1)
concrete event: command(arg0=c1)

abstract event: command(c_2)
concrete event: command(arg0=c2)

abstract event: success(c_1)
abstract event: success(arg0=c_1)

abstract event: success(c_2)
abstract event: success(arg0=c_2)

In this case study the assumption is that only the relevant information has been recorded.

--------------------------
c. Explanation
--------------------------

See README.txt for an overview of the case study this property is taken from.
