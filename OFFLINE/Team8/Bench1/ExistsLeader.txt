=============
ExistsLeader
=============

See README.txt for an overview.

--------------------------
a.i. Formal Representation
--------------------------

QEA:
exists leader, forall rover : leader!=rover

skip(1)[
  ping(leader,rover) -> 2
  ping(rover,leader) -> 1
]
skip(2)[
  ack(rover,leader) -> 3
]
skip(3)[]

accepts(3)

Note: In this formalisation the ping(rover,leader) event is
required due to the way that the domains of quantified variables
leader and rover are defined in QEA. This event means that the
domains of these variables are equal - so the set of rovers
consists of all objects that send or receive a ping.

Clarification: The original specification missed the global 
guard leader!=rover. There was also an inconsistency between
event names in this specification and those used in the logs.

--------------------------
a.ii. Informal Explanation
--------------------------

This property relates to the self-organisation of communicating rovers
and captures the situation where (at least) one rover is able to communicate
with all other (known) rovers.

In english the property states that there exists a leader (rover) who has
pinged every other (known) rover and received an acknowledgement.

Clarification: It is not necessary for the leader (rover) to ping itself.

Clarification: Communication is bidirectional in the sense that any rover
can ping any other rover (see example traces below).

Clarifcation: The following are examples of correct and incorrect traces.

Correct traces:
 - ping(A,B).ping(A,C).ack(B,A).ack(C,A)
 - ping(B,A).ping(A,B).ack(A,B)
 - ping(B,A).ping(A,C).ack(C,A).ping(B,C).ack(C,B).ack(A,B)
 - ping(A,A).ping(A,B).ack(B,A)

Incorrect traces:
 - ping(A,B).ping(A,C).ack(B,A).ping(B,D).ack(C,A)
 - ping(A,B).ping(B,C).ack(C,B).ack(B,A)
 - ping(A,B).ping(A,C).ping(A,D).ack(A,B).ack(C,B).ack(D,B)

--------------------------
a.iii. Verdict
--------------------------

The trace supplied satisfies this property i.e. evaluates to True.

--------------------------
b. Instrumentation information
--------------------------

Note that this specification requires single concrete events to be related to
multiple abstract events i.e. the concrete event
ack,arg0=981934525,arg1=743673026
will result in two abstract events, one for
leader=981934525,rover=743673026
and one for
leader=743673026,rover=981934525

abstract event: ping(leader,rover)
concrete event: ping(arg0=leader,arg1=rover)

abstract event: ping(rover,leader)
concrete event: ping(arg0=rover,arg1=leader)

abstract event: acknowledge(rover,leader)
concrete event: acknowledge(arg0=rover,arg1=leader)

In this case study the assumption is that only the relevant information has been recorded.

--------------------------
c. Explanation
--------------------------

See README.txt for an overview of the case study this property is taken from.

