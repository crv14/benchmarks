Trace Property Specification
       Duration (dur)
============================

Formal Representation
---------------------

For a linearly ordered sequence (T,<) of events e∈T, we define a predicate 
Failure⊆T comprising those events that exhibit a failure. The task is to compute 
this predicate. We assume T to give rise to mapping field: T × A → V that maps 
lines and attribute names A to values V. Further, we may use standard operators 
(such as <,=,+,-) on values, constants, variables x,y,z ranging over V and e,e' 
ranging over T. We omit explicit typing here for conciseness.

We define Failure such that for all events e∈T and all values (time stamps) x∈V

Failure(e) ⇔ 
  (field(e,"stop") = "true" ∧ 
   field(e,"time") = x ∧
   ∃e'<e:
     field(e',"start1") = true ∧
     (∀e'<e''<e: field(e'',"start1") ≠ "true" ∧ field(e'',"stop") ≠ "true") ∧
     x - field(e',"time") > 30000
  )

Explanation
-----------

Processes are started by start1 events. A stop event stops all running processes.
Failures are stop events that occur more than 30 seconds after most recent start1 event and since then no stop event has occurred.
That is, the difference of the values of the attribute “time” between two consecutive start1/stop events is supposed to be at most 30000 (milliseconds).

Verdict
-------
* Failure ≠ ∅, |Failure| = 47
* for all failing positions, see file spec2_dur-verdict.txt

Instrumentation Information
---------------------------
The provided log file includes a number of traces that are separated by events 
with name "log". That is between consecutive traces there is an entry such as the following:

<event>
  <name>log</name>
  <field><name>id</name><value>1</value></field>
</event>

Single positions within a trace are represented by events with name "step". The 
property is to be evaluated _separately_ on the traces represented in the log file.

The (partial) mapping field is interpreted by the trace given in terms of the log file.
Attribute names are the values enclosed by the <name> tag, values are enclosed by the <value> tag.
If, for some event e, some <field> tag encloses a pair of an attribute name a and a value v, then field(e,a) := v.

Whenever a field "start1" or "stop" is not explicitly specified in the log, the respective value is assumed to be false.


Remarks
-------
 
none
