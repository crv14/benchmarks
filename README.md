This GitLab project hosts the benchmarks used during the First International
Competition on Runtime Verification (CRV 14).

CRV 14 was held in September 2014, in Toronto, Canada, as a satellite event of
the 14th international conference on Runtime Verification (RV’14).

The event was organized in three tracks:
1. offline monitoring,
2. online monitoring of C programs, and
3. online monitoring of Java programs.
    
This repository hosts the benchmarks submitted by the participants and that were
used during the competition to evaluate the competing tools.

The repository can publicly be cloned (pull and push commands are not accessible).

The repository has the following structure:
* JAVA
* C
* OFFLINE

corresponding to the track organizations.

Then, each track directory has the following structure:
* Team1
* Team2
* ...

where each Teami directory contains directories related to the benchmarks submitted 
by Teami as described below. (The associations between team and indexes can be 
found in [1]).
* Bench1
* Bench2
* Bench3
* Bench4
* Bench5

A maximum of 5 benchmarks was allowed to the participating teams.



[1] First International Competition on Runtime Verification - Rules, Benchmarks,
Tools, and Final Results of CRV 2014. Ezio Bartocci, Yliès Falcone, Borzoo
Bonakdarpour, Christian Colombo, Normann Decker, Klaus Havelund, Yogi Joshi,
Felix Klaedtke, Reed Milewicz, Giles Reger, Grigore Rosu, Julien Signoles, Daniel
Thoma, Eugen Zalinescu, Yi Zhang. submitted to Software Tools for Technology Transfer.