=============
HashPersistance
=============


--------------------------
a.i. Formal Representation
--------------------------

QEA:
forall s, forall o

skip(1)[
  add(s,o) {/h=o.hashCode()} -> 2
]
next(2)[
  observe(s,o){h==o.hashCode()/} -> 2
  remove(s,o){h==o.hashCode()/} -> 1
  add(s,o){h==o.hashCode()/} -> 2
]

accepts(1,2)

Note: For this property it is assumed that object
identify is via System.IdentityHashCode() i.e.
reference equality, rather than semantic.

Note: This property also assumes the ability to
query the object using guards and assignments within
the property. Alternatively, the current hashCode could
be provided as part of the instrumentation.

Clarification: The initial state was incorrectly labelled
as a next state. It should be a skip state, to allow an
observe event to be called for an object not in the set.

Clarification: We had previously missed the case where an
object is added to a set twice. This has been added as a
looping transition on state 2.

--------------------------
a.ii. Informal Explanation
--------------------------

Objects placed in a hashing structure, such as a HashSet or HashMap
should have persistant hashcodes whilst in the structure for the
usage to be sound. Otherwise we may have the situation where we add
an object and then return false when checking for its presence.

--------------------------
a.iii. Verdict
--------------------------

On the DaCapo batik benchmark provided the verdict is True.

--------------------------
b. Instrumentation information
--------------------------

abstract event: add(s,o)
after(HashSet s, Object o) : call(* Collection+.add(Object)) && target(s) && args(o)
after(HashMap s, Object o) : call(* Map+.put(Object,Object)) && target(s) && args(o,*)

abstract event: observe(s,o)
after(HashSet s, Object o) : call(* Collection+.contains(Object)) && target(s) && args(o)
after(HashMap s, Object o) : call(* Map+.containsKey(Object)) && target(s) && args(o)
after(HashMap s, Object o) : call(* Map+.get(Object)) && target(s) && args(o)

abstract event: remove(s,o)
after(HashSet s, Object o) : call(* Collection+.remove(Object)) && target(s) && args(o)
after(HashMap s, Object o) : call(* Map+.remove(Object)) && target(s) && args(o)


--------------------------
c. Explanation
--------------------------

Note: To avoid monitoring the benchmark framework rather than the benchmark
program a pointcut should be used that excludes anything within org.dacapo
