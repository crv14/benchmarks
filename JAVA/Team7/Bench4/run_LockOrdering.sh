#!/bin/bash

# Benchmark four - LockOrdering for avrora
# Call with the tool you want to evaluate
# We use the once option as we are looking at time to first error

if [ "$#" -ne 1 ]; then
  echo "Supply a tool to evaluate"
  exit 1
fi

vi ../scripts && perl -w run.pl -tool $1 -once -specs LockOrdering -progs avrora
