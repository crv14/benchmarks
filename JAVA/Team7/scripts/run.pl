#!/usr/bin/perl

use strict 'refs';
use Getopt::Long;
use IPC::System::Simple qw(capture);

require "./config.pl";
require "./dacapofiles.pl";
use Config;

###########################
#
# Usage
#
# ./Run.pl -tool qea -specs HasNext FailSafeIter -progs fop
#
# If specs is empty it will use tooldir/default for each tool
#
# If progs is empty it will use all of them
#
# -orig runs original
#
#
###########################

my @Props = ();
my $tool;
my @Progs = ();

my $orig = 0;
my $size="default";
my $flag = "--no-validation -converge -window 5 ";
my $max = 1;
my $once = 0;
my $save = 0;
my $timeout = 60;


GetOptions(
	"specs:s{,}" => \@Props,
	"tools=s" => \$tool,
	"progs:s{,}" => \@Progs,
	'orig' => \$orig,
	"max:i" =>  \$max,
        "once" => \$once,
        "save" => \$save
	);

if($once){
	$flag = "--no-validation";
}
if($save){
	$flag = "$flag -preserve";
}

if(!@Progs){
    @Progs = ("avrora", "batik", "eclipse", "fop", "h2", "jython", "luindex", "lusearch", "pmd","sunflow", "tomcat", "tradebeans", "tradesoap", "xalan");

}

print "checking tool $tool...\n";
if(! -d "$home/$tool"){
	print "$tool not found\n";
	exit 0;
}
# If Props is empty get the default props
if(!@Props){
    if(! -e "$home/$tool/default"){
    	print "$home/$tool/default not found\n";
    	exit 0;
    }
    else{
	open DEFAULT_SPECS, "$home/$tool/default" or die $!;
	while (my $line = <DEFAULT_SPECS>){
		$line =~ s/\s+$//;
		if($line !~ /^\/\//){
			push(@Props,$line);
		}
   	 }						
    }	
}
	
my $ORIG="$dacapo/Dacapo9.12/original";
my $INSTR="$home/$tool/instrumented";
my $JARS="";
foreach $jar ( @{$Config::RT{$tool}} ){
	$JARS .= ":".$jar;
}
	
my $RES = "$home/$tool/results";
if(! -d $RES){
	mkdir($RES);
}

for ($count = 0; $count < $max; $count ++){
	mkdir("$RES/results_$count");
		
	foreach $prop (@Props)
	{
						
		foreach $jar ( @{$Config::RT{$tool}} ){
			system "cp $jar $tool/lib/";
		}
		
		$libs = "";
	
		@lib_files = glob "$home/$tool/lib/*.jar";
		foreach $lib_file (@lib_files){
			$libs .= ":".$lib_file;
		}
		$libs = substr $libs, 1;
					

		print "checking for property $prop...\n";
		foreach $prog (@Progs) {
			print "checking the program $prog...\n";
			$use_flag = $flag;
			if($save){
				$use_flag = "$flag -scratch-directory $home/$tool/$prop-$prog-scratch";
			}
			$ofile = "$RES/results_$count/".$prop."_".$prog."_".$size;
			$s = system "./timeout3 -t 3600 -i $timeout $JAVA -Xmx".$MAXMEM."M -cp \"$libs\":$INSTR/dacapo_$prop.jar Harness -s $size $use_flag $prog >$ofile.err 2>$ofile.results";
			print "checking the program $prog... done with $s\n";


		}
		print "checking for property $prop... done\n";
	}
}
if($orig)
{
	my $RES = "$home/orig";
	if(! -d $RES){
		mkdir($RES);
	}
	for ($count = 0; $count < $max; $count ++){
		mkdir("$RES/results_$count");
		print "running the original program...\n";
		foreach $prog (@Progs) {
			print "checking the program $prog...\n";
			$ofile = "$RES/results_$count/orig_".$prog."_".$size;
			system "\"$JAVA\" -Xmx".$MAXMEM."M -jar $dacapo/Dacapo9.12/original/dacapo-9.12-bach.jar -s $size $flag $prog > $ofile.err 2> $ofile.results";
		}
		print "running the original program...done!\n";
	}
}


