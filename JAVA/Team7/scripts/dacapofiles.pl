

	our @Files = (
	# avrora
	"avrora-cvs-20091224",
	# batik
	"batik-all", "xml-apis-ext","xml-apis", "crimson-1.1.3", "xerces_2_5_0", "xalan-2.6.0",
	# eclipse
	"eclipse",
	# fop
	"fop", "avalon-framework-4.2.0", "batik-all-1.7", "commons-io-1.3.1", "commons-logging-1.0.4", "serializer-2.7.0", "xmlgraphics-commons-1.3.1",
	# h2
	"derbyTesting","junit-3.8.1","h2-1.2.121","tpcc",
	# jython
	"jython", "antlr-3.1.3", "asm-3.1", "constantine-0.4", "jna-posix", "jna",
	# luindex
	"luindex","lucene-core-2.4","lucene-demos-2.4",
	# lusearch
	"lusearch",
	# pmd
	"pmd-4.2.5", "jaxen-1.1.1", "xercesImpl",
	# sunflow
	"sunflow-0.07.2", "janino-2.5.12",
	# tomcat
	"dacapo-tomcat", "bootstrap","tomcat-juli","commons-daemon","commons-httpclient", "commons-logging","commons-codec",
	# tradebeans, tradesoap
	"daytrader",
	# xalan
	"xalan-benchmark", "xalan", "serializer");