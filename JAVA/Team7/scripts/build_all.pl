#!/usr/bin/perl

# Building all
# For each tool folder in directory
# Build using default

opendir HERE, ".";
@tools = readdir HERE;
closedir HERE;
for $tool (@tools){

	#A folder is a tool folder if it has a default file
	if(-e "$tool/default"){
		print "Building for $tool";
		system "perl build.pl -tool $tool -makeAspects";
	}
}
