#!/usr/bin/perl

# Update with paths to the java and aspectj commands
# Aspectj stuff
$AJC="/opt/local/bin/ajc";
# Java stuff
$JAVAC="/usr/bin/javac";
$JAVA="/usr/bin/java";
$JAR="/usr/bin/jar";

$MAXMEM=6144; #  might need more!

# For locating things
$home=".";
$dacapo="$home/dacapo";
$aspects="$home/aspects";


package Config;
{
$home=".";
# Update with paths to the relevant libraries
$AJCRT="/opt/local/share/java/aspectjrt.jar";
$JAVAMOPRT="$home/lib/javamoprt.jar";
$QEART="$home/lib/qeart.jar";

$ABC_HOME="$home/lib/abc";

# for each tool define a list of jars that need to be included
	our %RT = (
	      base => ["$AJCRT"],
  	      javamop => ["$AJCRT","$JAVAMOPRT"],
  	      qea => ["$AJCRT","$QEART"]
    );


}


