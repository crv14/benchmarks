#!/usr/bin/perl

use strict 'refs';
use Getopt::Long;
use IPC::System::Simple qw(capture);

require "./config.pl";
require "./dacapofiles.pl";
use Config;

###########################
#
# Usage
#
# ./build.pl -tool qea  -specs HasNext UnsafeIter
#
# If specs is empty it will use tooldir/default
#
# makeAspects tells it to recompile the specs in the aspects directory
#
# abc tells it to use the abc compiler - Experimental, for use with tracematches
#
# results tells it to print out the result code of weaving - should be 0
#
###########################




my @props = ();
my $tool = ();
my $makeAspects = 0;
my $abc;
my $results;

GetOptions(
	"specs:s{,}" => \@props,
	"tools=s" => \$tool,
	'makeAspects' => \$makeAspects,
        'abc' => \$abc,
	'results' => \$results
	);

# If tools is empty use a default list
if (!$tool){
	print "You must provide a tool\n";
	exit 0;
}
if(! -d "$home/$tool"){
	print "$tool not found\n";
	exit 0;
}
print "Running for $tool\n";	

# If Props is empty get the default props
if(!@props){
    print "No specs given - loading default\n";
    if(! -e "$home/$tool/default"){
    	print "$home/$tool/default not found\n";
    	exit 0;
    }
    else{
	open DEFAULT_SPECS, "$home/$tool/default" or die $!;
	while (my $line = <DEFAULT_SPECS>){
		$line =~ s/\s+$//;
		if($line !~ /^\/\//){
			push(@props,$line);
		}
	}					
    }	
}
	
my $ORIG="$dacapo/Dacapo9.12/original";
my $INSTR="$home/$tool/instrumented";
if(! -d $INSTR){
	system "mkdir $INSTR";
}
my $SPECDIR="$aspects/$tool";
if(! -d $home/$tool/lib){
	system "mkdir $home/$tool/lib";
}
my $JARS="$home/$tool/lib/$tool-specs.jar";

foreach $jar ( @{$Config::RT{$tool}} ){
	$JARS .= ":".$jar;
}

# If makeSpecs selected or specsJar not there then build a specsJar and put in lib
if($makeAspects || ! -e "$home/$tool/lib/$tool-specs.jar"){

	print "Making specs jar\n";

	system "rm -f $tool/lib/$tool-specs.jar";

	mkdir("spec_temp");
	mkdir("spec_temp/$tool");
	system "rm -f $SPECDIR/*.class";

	if($abc){
		print "Experimental: using abc might not work!\n";

		system "java -classpath \"$JARS:$aspects\" -Xmx256M -Dabc.home=$ABC_HOME abc.main.Main -ext abc.tm -source 1.5 -debug dontCheckExceptions  $SPECDIR/*.aj -d $aspects";		
	}
	else{
		# in case we use extra java files
		if(glob("$SPECDIR/*.java")){
			system "$JAVAC -cp $aspects $SPECDIR/*.java -d $aspects";
		}
		# compile aspects in tool directory
		system "AJC -1.6 -Xjoinpoints:synchronization -cp \"$JARS:$aspects\" $SPECDIR/*.aj -d $aspects";	
	}
			
	system "cp $SPECDIR/*.class spec_temp/$tool";	
	system "cd spec_temp && $JAR cf $tool-specs.jar $tool";
	system "mv spec_temp/$tool-specs.jar $home/$tool/lib/$tool-specs.jar";
 	system "rm -rf spec_temp";

	#If we failed then exit
	if(! -e "$home/$tool/lib/$tool-specs.jar"){
		print "Failed to make aspects\n";
		exit 0;
	}
}
		
	# Build an instrumented version of DaCapo for each property 
foreach $prop (@props) {
	
	# This is the aspect I'm going to weave in
	# I get this by calling getAspect.pl from the tool directory as different tools
	# may construct this aspect differently
	
	if(! -e "$home/$tool/GetAspect.pl"){
		print "$home/$tool/GetAspect.pl not found\n";
		exit 0;			
	}
	
	$aspect = capture($^X, "$home/$tool/GetAspect.pl", "$prop");

	# get ready to weave dacapo
	
	system "rm -rf $INSTR/dacapo_$prop";
	system "rm -rf $INSTR/dacapo_$prop.jar";
	system "unzip -q $ORIG/dacapo-9.12-bach.jar -d $INSTR/dacapo_$prop";
	#We add the jars to the cnf files
	opendir CNFS, "$ORIG/cnf";
	@cnfs = readdir CNFS;
	closedir CNFS;
	foreach $cnf (@cnfs){
		open(IN_FILE, "< $ORIG/cnf/$cnf");
		open(OUT_FILE, "> $INSTR/dacapo_$prop/cnf/$cnf");
		
		while(<IN_FILE>){
			if($_ =~/libs (".*;")/){
				print OUT_FILE "  libs ";
				foreach $jar ( @{$Config::RT{$tool}} ){
					print OUT_FILE "\"$jar\",";
				}
				print OUT_FILE $1."\n";
			}
			else{ print OUT_FILE $_; }
		}
		close(IN_FILE);
		close(OUT_FILE);
	}


	system "cp $ORIG/MANIFEST.MF $INSTR/dacapo_$prop/META-INF/";
	system "cp $home/$tool/lib/$tool-specs.jar $INSTR/dacapo_$prop/jar/";
	foreach $jar ( @{$Config::RT{$tool}} ){
		system "cp $jar $INSTR/dacapo_$prop/jar/";
	}
			
	
	$dacapolibs = "";

	@lib_files = glob "$INSTR/dacapo_$prop/jar/*.jar";
	foreach $lib_file (@lib_files){
		$dacapolibs .= ":".$lib_file;
	}
	
	print "weaving dacapo with $prop...\n";


	if($abc){
		print "Experimental: Not sure if compiling with abc works\n";

		$s = system "java -classpath \"$JARS:$dacapolibs\" -Xmx256M -Dabc.home=$ABC_HOME abc.main.Main -ext abc.tm -source 1.5 -debug dontCheckExceptions -inpath $INSTR/dacapo_$prop $SPECDIR/$aspect -d $INSTR/dacapo_$prop/ >$home/build-logs/build_dacapo_$tool_$prop.log 2>$home/build-logs/build_dacapo_$tool_$prop.err.log";
		if($results){print $s."\n";}			
	}
	else{
		$s = system "$AJC -Xjoinpoints:synchronization -1.6 -cp \"$JARS:$dacapolibs\" -showWeaveInfo -inpath $INSTR/dacapo_$prop $SPECDIR/$aspect -d $INSTR/dacapo_$prop/ >$home/build-logs/build_dacapo_$tool_$prop.log 2>$home/build-logs/build_dacapo_$tool_$prop.err.log";
		if($results){print $s."\n"};
	}

	print "weaving libs with $prop...\n";

	$ORIG_LIB = "$dacapo/Dacapo9.12/lib";
	
	foreach $file (@Files){
		print "-$file\n";
	
		system "rm -rf $INSTR/dacapo_$prop/jar/$file";
		system "unzip -q $INSTR/dacapo_$prop/jar/$file.jar -d $INSTR/dacapo_$prop/jar/$file";
	
		$runlib = "";
		if(-e "$ORIG_LIB/$file"){
			@lib_files = glob "$ORIG_LIB/$file/*.jar";
			foreach $lib_file (@lib_files){
				$runlib .= ":".$lib_file;
			}
	
		} else {
			@lib_files = glob "$ORIG_LIB/*.jar";
			foreach $lib_file (@lib_files){
				$runlib .= ":".$lib_file;
			}
		}
	
		if($abc){
			$s = system "java -classpath \"$JARS:$INSTR/dacapo_$prop/:$runlib:$dacapolibs\" -Xmx2GB -Dabc.home=$ABC_HOME abc.main.Main -ext abc.tm -source 1.5 -debug dontCheckExceptions -inpath $ORIG/dacapo-9.12-bach/jar/$file.jar $SPECDIR/$aspect -d $INSTR/dacapo_$prop/jar/$file >$home/build-logs/build_dacapo_$tool_$prop"."_$file.log 2>$home/build-logs/build_dacapo_$tool_$prop"."_$file.err.log";
			if($results){print $s."\n";}
		}
		else{
	
			$s = system "$AJC -Xjoinpoints:synchronization -1.6 -cp \"$JARS:$INSTR/dacapo_$prop/:$runlib:$dacapolibs\" -showWeaveInfo -inpath $ORIG/dacapo-9.12-bach/jar/$file.jar $SPECDIR/$aspect -d $INSTR/dacapo_$prop/jar/$file >$home/build-logs/build_dacapo_$tool_$prop"."_$file.log 2>$home/build-logs/build_dacapo_$tool_$prop"."_$file.err.log";
			if($results){print $s."\n";}
		}
		if(-e "$INSTR/dacapo_$prop/jar/$file/META-INF/MANIFEST.MF"){
			system "$JAR cmf $INSTR/dacapo_$prop/jar/$file/META-INF/MANIFEST.MF $INSTR/dacapo_$prop/jar/$file.jar -C $INSTR/dacapo_$prop/jar/$file .";
		}else{

			system "$JAR cf $INSTR/dacapo_$prop/jar/$file.jar -C $INSTR/dacapo_$prop/jar/$file .";
		}
		# remove folder now we have the jar
		system "rm -rf $INSTR/dacapo_$prop/jar/$file";
	
	}

	#Wrap it all up!	
	system "$JAR cmf $ORIG/MANIFEST.MF $INSTR/dacapo_$prop.jar -C $INSTR/dacapo_$prop .";
	system "rm -rf $INSTR/dacapo_$prop";
	print "weaving dacapo with $tool for $prop ...done!\n";
				
}

