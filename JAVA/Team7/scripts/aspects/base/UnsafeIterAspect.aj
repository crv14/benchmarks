package base;

import java.util.*;
import java.io.*;

public aspect UnsafeIterAspect{

	public static pointcut scope() : !within(org.dacapo..*) && !within(java..*) && !within(org.apache.commons..*) && !within(com.sun..*) && !within(sun..*) && !within(sunw..*);

	pointcut collection_updateD(Collection c) :
	        (  call(* java.util.Collection+.add*(..)) ||
	           call(* java.util.Collection+.remove*(..)) ) && target(c);
	   
	      
    @SuppressWarnings("unchecked")
    after(Collection c)  returning(Iterator i) : ( call(* java.util.Collection+.iterator()) && target(c)) && scope() {
	System.err.println("iterator("+System.identityHashCode(c)+","+System.identityHashCode(i)+")");
    }

    @SuppressWarnings("unchecked")
    before(Iterator i) :( call(* java.util.Iterator+.next()) && target(i)) && scope() {
	System.err.println("use("+System.identityHashCode(i)+")");
    }
    @SuppressWarnings("unchecked")
    before(Collection c) :( collection_updateD(c)) && scope() {
	System.err.println("update("+System.identityHashCode(c)+")");
    }


}


