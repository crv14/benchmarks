package base;

import java.util.Iterator;

public aspect HasNextAspect {

	public static pointcut scope() : !within(org.dacapo..*) && !within(java..*) && !within(org.apache.commons..*) && !within(com.sun..*) && !within(sun..*) && !within(sunw..*);

	@SuppressWarnings("unchecked")
	pointcut hasNext(Iterator i) : call(* java.util.Iterator+.hasNext()) && target(i);
	@SuppressWarnings("unchecked")
	pointcut next(Iterator i) :  call(* java.util.Iterator+.next()) && target(i);	
		
	@SuppressWarnings("unchecked")
	after(Iterator i) returning(boolean b): hasNext(i) && scope()  {
		System.err.println("hasnext("+System.identityHashCode(i)+","+b+")");
        }
        @SuppressWarnings("unchecked")
    	before(Iterator i) : next(i) && scope() {
		System.err.println("next("+System.identityHashCode(i)+")");
        }


}


