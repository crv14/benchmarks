package base;

import java.io.*;
import java.util.*;

public aspect PersistentHashesAspect {


	public static pointcut scope() : !within(org.dacapo..*) && !within(java..*) && !within(org.apache.commons..*) && !within(com.sun..*) && !within(sun..*) && !within(sunw..*);
		
	@SuppressWarnings("unchecked")
	after(HashSet t, Object o) : call(* Collection+.add(Object)) && target(t) && args(o) && scope()  {
		System.err.println("add("+System.identityHashCode(t)+","+System.identityHashCode(o)+","+o.hashCode()+")");
        }
	@SuppressWarnings("unchecked")
	after(HashMap t, Object o) : call(* Map+.put(Object,Object)) && target(t) && args(o,*) && scope()  {
		System.err.println("add("+System.identityHashCode(t)+","+System.identityHashCode(o)+","+o.hashCode()+")");
        }

        @SuppressWarnings("unchecked")
    	after(HashSet t, Object o) : call(* Collection+.contains(Object)) && target(t) && args(o) && scope()  {
		System.err.println("observe("+System.identityHashCode(t)+","+System.identityHashCode(o)+","+o.hashCode()+")");
        }

        @SuppressWarnings("unchecked")
    	after(HashMap t, Object o) : call(* Map+.containsKey(Object)) && target(t) && args(o) && scope()  {
		System.err.println("observe("+System.identityHashCode(t)+","+System.identityHashCode(o)+","+o.hashCode()+")");
        }

        @SuppressWarnings("unchecked")
    	after(HashMap t, Object o) : call(* Map+.get(Object)) && target(t) && args(o) && scope()  {
		System.err.println("observe("+System.identityHashCode(t)+","+System.identityHashCode(o)+","+o.hashCode()+")");
        }


        @SuppressWarnings("unchecked")
    	after(HashSet t, Object o) : call(* Collection+.remove(Object)) && target(t) && args(o) && scope()  {
		System.err.println("remove("+System.identityHashCode(t)+","+System.identityHashCode(o)+","+o.hashCode()+")");
        }

        @SuppressWarnings("unchecked")
    	after(HashMap t, Object o) : call(* Map+.remove(Object)) && target(t) && args(o) && scope()  {
		System.err.println("remove("+System.identityHashCode(t)+","+System.identityHashCode(o)+","+o.hashCode()+")");
        }


}


