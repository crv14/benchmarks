package base;

import java.util.*;

public aspect SafeIteratorAspect {


	public static pointcut scope() : !within(org.dacapo..*) && !within(java..*) && !within(org.apache.commons..*) && !within(com.sun..*) && !within(sun..*) && !within(sunw..*);

	@SuppressWarnings("unchecked")
	pointcut next(Iterator i) :  call(* java.util.Iterator+.next()) && target(i);	
		
	@SuppressWarnings("unchecked")
	after(Collection c)  returning(Iterator i) : ( call(* java.util.Collection+.iterator()) && target(c)) && scope() {
		int size = c.size();
		System.err.println("iterator("+System.identityHashCode(c)+","+System.identityHashCode(i)+","+size+")");
        }
        @SuppressWarnings("unchecked")
    	before(Iterator i) : next(i) && scope() {
		System.err.println("next("+System.identityHashCode(i)+")");
        }



}


