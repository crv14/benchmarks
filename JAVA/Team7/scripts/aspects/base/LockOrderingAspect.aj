package base;

import java.util.*;
import java.io.*;

public aspect LockOrderingAspect {


	public static pointcut scope() : !within(org.dacapo..*) && !within(java..*) && !within(org.apache.commons..*) && !within(com.sun..*) && !within(sun..*) && !within(sunw..*);

	@SuppressWarnings("unchecked")
	before(Object l) : lock() && args(l) && scope() {
		System.err.println("lock("+System.identityHashCode(l)+")");
    	}

	@SuppressWarnings("unchecked")
	before(Object l) : unlock() && args(l) && scope() {
		System.err.println("unlock("+System.identityHashCode(l)+")");
    	}
  

}


