#!/bin/bash


TESTSDIR=$@
TESTCASES='call01.asm example.asm infinite01.asm int01.asm loop01.asm'


for (( i = 1; i <= 3; i++ ))
do
  echo "Running all tests for $i times"
  for t in $TESTCASES; do
    java -cp bin/ avrora.Main -action=cfg $TESTSDIR/$t
  done
done

