#!/bin/bash

if [[ ! -d jars ]]; then
    mkdir jars
fi

cd bin

JARFILE=avrora.jar

jar cmf MANIFEST.MF ../jars/$JARFILE avrora cck
if [ ! "$?" = 0 ]; then
    echo "  -> Error: could not build jar file $JARFILE."
    exit 1
fi

echo $JARFILE
