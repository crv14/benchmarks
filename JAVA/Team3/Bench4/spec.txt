Program Property Specification
          Velocity
============================

Formal Representation
---------------------
G((time = t ∧ pos = s) ⇒ WX(pos - s < 10 · time - t)))

s, t: Implicitly universially quantified variables
time, pos: Observation symbols, constants

Explanation
-----------
The average speed between two observations (i.e. calls to step) must never exceed the maximal velocity 10.

Verdict
-------
true

Instrumentation Information
---------------------------
The observed event is the call to method step.
The observation symbol time refers to the time parameter of method step.
The observation symbol pos refers to the time parameter of method step.

Remarks
-------
none
