Specification formalism
========================
As specification formalism, we use LTL over first-order formulae.
Free variables are assumed to be universally quantified at the outermost position.
First-order symbols either refer to some fixed theory or to the current system observation (called observation symbols).
In our examples we only use the theories of IDs, linear equations over integers and linear equations over reals.
A detailed description of the formalism can be found in [DLT14].

References
----------
[DLT14] N. Decker, M. Leucker, D. Thoma: Monitoring modulo theories. TACAS 2014
        http://www.isp.uni-luebeck.de/research/publications/monitoring-modulo-theories
