Program Information
          Locking
============================

Explanation
-----------
The program ExampleLocking runs multiple threads in parallel performing some critical action.
The critical action must only be performed by one thread as a time.
Thus, the threads have to call the method boolean lock() (with return value true) before calling void action().
The lock is released by calling void unlock().

Compile
-------
javac rvcomp/ExampleLocking.java

Run
---
java -cp . rvcomp.ExampleLocking
