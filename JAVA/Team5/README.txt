These benchmarks describe finite-state machines, encoded as Java code (see the respective FSM*.java) files. The accompanying .aj files are used to generate the appropriate events using AspectJ aspects.

The benchmark properties and the prm4j tool itself are further described in Mateusz Parzonkas Master thesis, a copy of which you find in this directory.

Instructions on how to run the benchmarks with prm4j are found here:
https://github.com/parzonka/prm4j-eval/