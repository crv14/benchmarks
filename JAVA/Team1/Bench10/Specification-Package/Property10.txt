<<Formal representation>>

<guarded command language>

UserSession.openSession(..) target (UserSession s) | -> { Verification.openSessions.add(s); }
UserSession.closeSession(..) target (UserSession s) | -> { Verification.openSessions.remove(s); }
UserSession.log(..) target (UserSession s)
  |  !Verification.openSessions.contains(s)
  -> { Verification.fail("P10 violated"); }

<finite state machine>

property foreach target (UserSession s) starting LoggedOut {
    LoggedOut >>> UserSession.openSession(..) target (UserSession s) 
              >>> LoggedIn       
                 
    LoggedIn >>> UserSession.closeSession(..) target (UserSession s) 
             >>> LoggedOut
          
    LoggedOut >>> UserSession.log(..) target (UserSession s)
              >>> Bad[P10 violated] 
}




<non matching regular expression>
property foreach target (UserSession u) not matching{
(!openSession)*;(openSession;(!closeSession)*;closeSession)*;(!openSession)*;log
}



<<Informal explanation>>

Logging can only be made to an active session (i.e. between a login and a logout).


<<Verdict>>

A violation should be detected in scenario 19

