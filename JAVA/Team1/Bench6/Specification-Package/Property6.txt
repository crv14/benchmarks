<<Formal representation>>

<guarded command language>

UserInfo.greylist(..) target (UserInfo u) | -> { 
	Verification.countTransfers.put(u,0);
}
UserInfo.depositTo(..) target (UserInfo u) | -> { 
	if (!Verification.countTransfers.containsKey(u)) {
		Verification.countTransfers.put(u,0);
	}
	Verification.countTransfers.put(u,Verification.countTransfers.get(u)+1); 
}
UserInfo.whitelist(..) target (UserInfo u) 
  |  ( Verification.countTransfers.containsKey(u) && Verification.countTransfers.get(u) < 3 )
  -> { Verification.fail("P6 violated"); }



<matching regular expression>

property foreach target (UserInfo u) matching{
((!greylist)* ; greylist ; ((!whitelist)* ; depositTo ; (!whitelist)* ; depositTo ; (!whitelist)* ; depositTo ; (!whitelist)*) ; whitelist )*
}



<non matching regular expression>

property foreach target (UserInfo u) not matching{
(?)* ; greylist; ((!depositTo)* + (!depositTo)*;depositTo;(!depositTo)* + (!depositTo)*;depositTo;(!depositTo)*;depositTo;(!depositTo)*) ; whitelist
}



<<Informal explanation>>

Once greylisted, a user must perform at least three incoming transfers before being whitelisted.


<<Verdict>>

A violation should be detected in scenario 11

