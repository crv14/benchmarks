<<Formal representation>>

<guarded command language>

UserInfo.openSession(..) target (UserInfo u) | -> {
  if (!Verification.countSessions.containsKey(u)) { Verification.countSessions.put(u,0); }
  Verification.countSessions.put(u,Verification.countSessions.get(u)+1); 
  if (Verification.countSessions.get(u) > 3) { Verification.fail("P9 violated"); }
}
UserInfo.closeSession(..) target (UserInfo u) | -> {
  Verification.countSessions.put(u,Verification.countSessions.get(u)-1); 
}



<<Informal explanation>>

A user may not have more than 3 active sessions at once.


<<Verdict>>

A violation should be detected in scenario 17

