The financial transaction system (FiTS) is a simple system which allows users to perform a number of transactions. 
More details are available in the manual.

The system has 10 specified properties and executes 2 scenarios focusing on each property (20 scenarios in all): one violating the property and one not. 