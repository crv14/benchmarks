// RUN: test.sh -e -t %t %s
//
// TEST: buffer-001
//
// Description:
//  Test that an off-by-one read on a global is detected.
//
#include <stdio.h>
#include <stdlib.h>
void v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(unsigned long long input1,unsigned long input2);

struct __Pb__Cc__Pe___Type 
{
  const char *ptr;
  unsigned long long addr;
}
;
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(unsigned long long input1,unsigned long long input2,unsigned long input3,unsigned long long input4);

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_create_struct___Pb__Cc__Pe___Arg_UL_Arg(const char *input1,unsigned long long input2)
{
  struct __Pb__Cc__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(const char *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return __Pb__Cc__Pe___Type_Ret_create_struct___Pb__Cc__Pe___Arg_UL_Arg(input1,input2);
}

static const char *__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(struct __Pb__Cc__Pe___Type input1)
{
  return input1 . ptr;
}
void v_Ret_create_dummy_entry_UL_Arg(unsigned long long input1);
void v_Ret_execAtFirst();
void v_Ret_execAtLast();
static char *array;
static char* global_var1_17_10 = "the value is %d\n";

int main(int argc,char **argv)
{
  v_Ret_execAtFirst();
  v_Ret_create_dummy_entry_UL_Arg(((unsigned long long )(&argv)));
  int index_argc;
  for (index_argc = 0; index_argc < argc; ++index_argc) 
    v_Ret_create_dummy_entry_UL_Arg(((unsigned long long )(&argv[index_argc])));
  int i_index_0;
  (i_index_0 = 1024 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&array)),((unsigned long )(&array[i_index_0]))));
  int value = array[i_index_0];
  printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var1_17_10),((unsigned long long )(&global_var1_17_10)),((unsigned long )(sizeof(global_var1_17_10))),((unsigned long long )50))),value);
  v_Ret_execAtLast();
  return 0;
}
