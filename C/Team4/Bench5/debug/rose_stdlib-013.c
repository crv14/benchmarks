/* strcat() with negative pointer index */
#include <string.h>
#include <stdlib.h>

struct __Pb__v__Pe___Type 
{
  void *ptr;
  unsigned long long addr;
}
;
struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_malloc_overload_Ul_Arg(unsigned long input1);

struct __Pb__c__Pe___Type 
{
  char *ptr;
  unsigned long long addr;
}
;

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input1)
{
  struct __Pb__c__Pe___Type output;
  output . ptr = ((char *)input1 . ptr);
  output . addr = input1 . addr;
  return output;
}
void v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(unsigned long long input1,unsigned long long input2);

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(char *input1,unsigned long long input2)
{
  struct __Pb__c__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(char **input1,struct __Pb__c__Pe___Type input2)
{
   *input1 = input2 . ptr;
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )input1),((unsigned long long )input2 . addr));
  return __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(input2 . ptr,((unsigned long long )input1));
}
void v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(unsigned long long input1,unsigned long input2);
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(unsigned long long input1,unsigned long long input2,unsigned long input3,unsigned long long input4);

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_AddressOf_UL_Arg___Pb__c__Pe___Arg_Ul_Arg_UL_Arg(unsigned long long input1,char *input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input1,((unsigned long long )input2),input3,input4);
  return __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(input2,input1);
}
static char* global_var3_12_18 = "This is more than 10";

struct __Pb__Cc__Pe___Type 
{
  const char *ptr;
  unsigned long long addr;
}
;

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_create_struct___Pb__Cc__Pe___Arg_UL_Arg(const char *input1,unsigned long long input2)
{
  struct __Pb__Cc__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(const char *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return __Pb__Cc__Pe___Type_Ret_create_struct___Pb__Cc__Pe___Arg_UL_Arg(input1,input2);
}

static char *__Pb__c__Pe___Ret_return_pointer___Pb__c__Pe___Type_Arg(struct __Pb__c__Pe___Type input1)
{
  return input1 . ptr;
}

static const char *__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(struct __Pb__Cc__Pe___Type input1)
{
  return input1 . ptr;
}
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(unsigned long long input1,unsigned long long input2,unsigned long input3);

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_create_struct_from_addr_UL_Arg(unsigned long long input1)
{
  struct __Pb__c__Pe___Type output;
  output . ptr =  *((char **)input1);
  output . addr = input1;
  return output;
}

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(char *input1,unsigned long long input2)
{
  struct __Pb__v__Pe___Type output;
  output . ptr = ((void *)input1);
  output . addr = input2;
  return output;
}
void v_Ret_free_overload___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input1);
void v_Ret_execAtFirst();
void v_Ret_execAtLast();

int main()
{
  v_Ret_execAtFirst();
  char *c;
  char *x;
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&c,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_malloc_overload_Ul_Arg(((unsigned long )10))));
  int i_index_0;
  (i_index_0 = 0 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&c)),((unsigned long )(&c[i_index_0]))));
  c[i_index_0] = '\0';
  int i_index_1;
  (i_index_1 = 2 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&c)),((unsigned long )(&c[i_index_1]))));
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&x,__Pb__c__Pe___Type_Ret_AddressOf_UL_Arg___Pb__c__Pe___Arg_Ul_Arg_UL_Arg(((unsigned long long )1),&c[i_index_1],((unsigned long )(sizeof(c[i_index_1]))),((unsigned long long )50)));
  int i_index_2;
  (i_index_2 = -2 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&x)),((unsigned long )(&x[i_index_2]))));
  char *__Pb__c__Pe___libcall_ret_5;
  ((__Pb__c__Pe___libcall_ret_5 = strcat(__Pb__c__Pe___Ret_return_pointer___Pb__c__Pe___Type_Arg(__Pb__c__Pe___Type_Ret_AddressOf_UL_Arg___Pb__c__Pe___Arg_Ul_Arg_UL_Arg(((unsigned long long )2),&x[i_index_2],((unsigned long )(sizeof(x[i_index_2]))),((unsigned long long )50))),__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var3_12_18),((unsigned long long )(&global_var3_12_18)),((unsigned long )(sizeof(global_var3_12_18))),((unsigned long long )50)))) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__c__Pe___libcall_ret_5)),((unsigned long long )__Pb__c__Pe___libcall_ret_5),((unsigned long )(sizeof(__Pb__c__Pe___libcall_ret_5))))) , __Pb__c__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__c__Pe___libcall_ret_5))));
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(c,((unsigned long long )(&c))));
  v_Ret_execAtLast();
  return 0;
}
