unsigned long long UL_Ret_get_from_stack_i_Arg(int input1);
void v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(unsigned long long input1,unsigned long long input2);

struct __Pb__Cc__Pe___Type 
{
  const char *ptr;
  unsigned long long addr;
}
;

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(char *input1,unsigned long long input2)
{
  struct __Pb__Cc__Pe___Type output;
  output . ptr = ((const char *)input1);
  output . addr = input2;
  return output;
}

static const char *__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(struct __Pb__Cc__Pe___Type input1)
{
  return input1 . ptr;
}
void v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(unsigned long long input1,unsigned long input2);

struct __Pb__v__Pe___Type 
{
  void *ptr;
  unsigned long long addr;
}
;

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(char *input1,unsigned long long input2)
{
  struct __Pb__v__Pe___Type output;
  output . ptr = ((void *)input1);
  output . addr = input2;
  return output;
}

static void *__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input1)
{
  return input1 . ptr;
}
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(unsigned long long input1,unsigned long long input2,unsigned long input3);

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(unsigned long long input1)
{
  struct __Pb__v__Pe___Type output;
  output . ptr =  *((void **)input1);
  output . addr = input1;
  return output;
}

struct __Pb__c__Pe___Type 
{
  char *ptr;
  unsigned long long addr;
}
;

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_create_struct_from_addr_UL_Arg(unsigned long long input1)
{
  struct __Pb__c__Pe___Type output;
  output . ptr =  *((char **)input1);
  output . addr = input1;
  return output;
}
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(unsigned long long input1,unsigned long long input2,unsigned long input3,unsigned long long input4);

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(char *input1,unsigned long long input2)
{
  struct __Pb__c__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(char *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(input1,input2);
}

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(char **input1,struct __Pb__c__Pe___Type input2)
{
   *input1 = input2 . ptr;
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )input1),((unsigned long long )input2 . addr));
  return __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(input2 . ptr,((unsigned long long )input1));
}

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_create_struct___Pb__Cc__Pe___Arg_UL_Arg(const char *input1,unsigned long long input2)
{
  struct __Pb__Cc__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(const char *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return __Pb__Cc__Pe___Type_Ret_create_struct___Pb__Cc__Pe___Arg_UL_Arg(input1,input2);
}
struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_malloc_overload_Ul_Arg(unsigned long input1);

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input1)
{
  struct __Pb__c__Pe___Type output;
  output . ptr = ((char *)input1 . ptr);
  output . addr = input1 . addr;
  return output;
}

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_create_struct___Pb__v__Pe___Arg_UL_Arg(void *input1,unsigned long long input2)
{
  struct __Pb__v__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(void *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return __Pb__v__Pe___Type_Ret_create_struct___Pb__v__Pe___Arg_UL_Arg(input1,input2);
}

static int i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(char *input1,struct __Pb__c__Pe___Type input2)
{
  return input1 == input2 . ptr;
}
void v_Ret_push_to_stack_UL_Arg(unsigned long long input1);
unsigned long long UL_Ret_pop_from_stack_i_Arg(int input1);
void v_Ret_free_overload___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input1);
void v_Ret_create_dummy_entry_UL_Arg(unsigned long long input1);
void v_Ret_execAtFirst();
void v_Ret_execAtLast();
static char *data_rights_legend;
/********************************************************************
 ** This software (or technical data) was produced for the U. S.
 ** Government under contract 2009-0917826-016 and is subject to 
 ** the Rights in Data-General Clause 52.227-14. Alt. IV (DEC 2007).
 ** 
 ** (c) Copyright 2012 The MITRE Corporation. All Rights Reserved.
 ********************************************************************/
/*******************************************
**
**
** 
** Date: 08/08/2011
**
** Base Test Program -- TheNextPalindromeC
**
** This Base Test Program finds the next highest number that is a
** palindrome after the given number.
**
**
** Variant Test Case Program
**
** This program has been modified by adding a heap-based buffer
** overflow when reading in the number to find the next palindrome.
** The number is written with an unbounded string copy to the heap
** allocated buffer 'k' of size MAX_LEN (300).  This could allow an
** attacker to execute code by writing past the buffer and overwriting
** function pointers that exist in memory.
**
**
** STONESOUP Weakness Class: Buffer Overflows and Memory Corruption
** CWE ID: CWE-122 Heap-based Buffer Overflow
** Variant Spreadsheet Rev #: ###
** Variant Spreadsheet ID: 1016
**
** Variant Features:
**		SOURCE_TAINT:COMMAND_LINE
**		DATA_TYPE:HEAP_POINTER
**		CONTROL_FLOW:LOOP_COMPLEXITY_TEST
**		DATA_FLOW:PASS_BY_REFERENCE
**
** Initial Testing: (x means yes, - means no)
**   Tested in MS Windows XP 32bit        x
**   Tested in MS Windows 7  64bit        -
**   Tested in Ubuntu10_10 Linux 32bit    -
**   Tested in Ubuntu10_10 Linux 64bit    -
**
** Workflow:
**   Created: 08/08/2011
**   1st Vett:  on 08/08/2011
**   2nd Vett: <peer> on <date>
**   3rd Vett: <teamleader> on <date>
**   4th Vett: Fortify Issues Fixed on <date>
**   5th Vett: Tested in Harness on <date>
**
**
** I/0 Pairs:
**   Good: 1st Set:
**         2nd Set:
**         3rd Set:
**         4th Set:
**         5th Set:
**    Bad: 1st Set:
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#define MAX_LEN		300
//Checks a character array. Returns 1 if it only contains ints, 0 otherwise.
int isAllInts(char *k);
//Checks a character array. Returns 1 if it only contains 9s, 0 otherwise.
int isAllNines(char *k);
//Adds two integers, a & b represented by character strings. Puts the resultant value into ret.
void add(char *a,char *b,char *ret,int ret_len);
//substring
void substr(char *src,int iStart,int iLen,char *dst);
//Reverses string k
void reverse(char *src,char *dst,int len);
//Returns TRUE if the integer represented by string left is GREATER THAN the integer represented by right
int greaterThan(char *left,int left_len,char *right,int right_len);

int isAllInts(char *k)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&k)),UL_Ret_get_from_stack_i_Arg(0));
  int i = 0;
  int v;
  int k_len = (strlen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))))));
  for (i = 0; i < k_len; i++) {
    int i_index_0;
    (i_index_0 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&k)),((unsigned long )(&k[i_index_0]))));
    v = k[i_index_0];
    if (v < 48 || v > 57) {
      return 0;
    }
  }
  return 1;
}
//substring function

void substr(char *src,int iStart,int iLen,char *dst)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&dst)),UL_Ret_get_from_stack_i_Arg(1));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&src)),UL_Ret_get_from_stack_i_Arg(0));
  int i;
//zero the destination array
  void *__Pb__v__Pe___libcall_ret_1;
  ((__Pb__v__Pe___libcall_ret_1 = memset(__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(dst,((unsigned long long )(&dst)))),0,(iLen + 1)) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_1)),((unsigned long long )__Pb__v__Pe___libcall_ret_1),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_1))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_1))));
  for (i = 0; i < iLen; i++) {
    int i_index_2;
    (i_index_2 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&dst)),((unsigned long )(&dst[i_index_2]))));
    int i_index_3;
    (i_index_3 = i + iStart , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&src)),((unsigned long )(&src[i_index_3]))));
    dst[i_index_2] = src[i_index_3];
  }
  int i_index_4;
  (i_index_4 = iLen , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&dst)),((unsigned long )(&dst[i_index_4]))));
  dst[i_index_4] = '\0';
}
//Determines if the input string consists of only the number 9.

int IsAllNines(char *k)
{
//TODO: Should i ask for the length of K here?
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&k)),UL_Ret_get_from_stack_i_Arg(0));
  unsigned int x;
  unsigned int len_k = (strlen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))))));
  for (x = 0; x < len_k; x++) {
    unsigned int Ui_index_5;
    (Ui_index_5 = x , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&k)),((unsigned long )(&k[Ui_index_5]))));
    if (k[Ui_index_5] - 48 != 9) {
      return 0;
    }
  }
  return 1;
}
//This function adds two integers represented as strings and returns the sum, represented as a string
//ret MUST BE equal to Max(len a, len b) + 2

void add(char *a,char *b,char *ret,int ret_len)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&ret)),UL_Ret_get_from_stack_i_Arg(2));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&b)),UL_Ret_get_from_stack_i_Arg(1));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&a)),UL_Ret_get_from_stack_i_Arg(0));
  int a_len = (strlen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(a,((unsigned long long )(&a))))));
  int b_len = (strlen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(b,((unsigned long long )(&b))))));
  int carry = 0;
  int pos = 1;
  void *__Pb__v__Pe___libcall_ret_6;
  ((__Pb__v__Pe___libcall_ret_6 = memset(__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(ret,((unsigned long long )(&ret)))),0,(ret_len + 1)) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_6)),((unsigned long long )__Pb__v__Pe___libcall_ret_6),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_6))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_6))));
//Iterate right to left, adding as we go
  while(1){
    if (!(pos <= a_len || pos <= b_len || carry != 0)) 
      break; 
    unsigned int a_int;
    unsigned int b_int;
    unsigned int sum;
//printf("pos: %i\n", pos);
    int i_index_7;
    (i_index_7 = a_len - pos , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&a)),((unsigned long )(&a[i_index_7]))));
    a_int = (((pos > a_len?48 : a[i_index_7])) - 48);
    int i_index_8;
    (i_index_8 = b_len - pos , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&b)),((unsigned long )(&b[i_index_8]))));
    b_int = (((pos > b_len?48 : b[i_index_8])) - 48);
    sum = a_int + b_int + carry;
    int i_index_9;
    (i_index_9 = ret_len - pos , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&ret)),((unsigned long )(&ret[i_index_9]))));
    ret[i_index_9] = (sum % 10 + 48);
    carry = ((sum - sum % 10) / 10);
    pos++;
  }
//We have completed adding A and B. The sum is in *ret
//However, the sum may or may not begin in ret[0]. If the last execution of the WHILE loop
//resulted in a CARRY (as would happen for 9222 + 9222), ret[0] will be non-null.
//If the last execution of the WHILE loop did not result in a carry (as would happen for 111 + 111),
//ret[0] will be null.
//If ret[0] is null, we need to shift the data to the left by one. This will be done by ret[0] = ret[1] ... ret[n] = ret[n + 1];
  int i_index_10;
  (i_index_10 = 0 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&ret)),((unsigned long )(&ret[i_index_10]))));
  if (ret[i_index_10] == '\0') {
    int i;
    for (i = 0; i + 1 < ret_len; i++) {
      int i_index_11;
      (i_index_11 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&ret)),((unsigned long )(&ret[i_index_11]))));
      int i_index_12;
      (i_index_12 = i + 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&ret)),((unsigned long )(&ret[i_index_12]))));
      ret[i_index_11] = ret[i_index_12];
    }
    int i_index_13;
    (i_index_13 = ret_len - 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&ret)),((unsigned long )(&ret[i_index_13]))));
    ret[i_index_13] = '\0';
  }
}
//Determines if the input string consists of only the number 9.

int isAllNines(char *k)
{
//TODO: Should i ask for the length of K here?
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&k)),UL_Ret_get_from_stack_i_Arg(0));
  unsigned int x;
  unsigned int len_k = (strlen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))))));
  for (x = 0; x < len_k; x++) {
    unsigned int Ui_index_14;
    (Ui_index_14 = x , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&k)),((unsigned long )(&k[Ui_index_14]))));
    if (k[Ui_index_14] - 48 != 9) {
      return 0;
    }
  }
  return 1;
}
//Reverses string k

void reverse(char *src,char *dst,int len)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&dst)),UL_Ret_get_from_stack_i_Arg(1));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&src)),UL_Ret_get_from_stack_i_Arg(0));
  int i;
  void *__Pb__v__Pe___libcall_ret_15;
  ((__Pb__v__Pe___libcall_ret_15 = memset(__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(dst,((unsigned long long )(&dst)))),0,(len + 1)) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_15)),((unsigned long long )__Pb__v__Pe___libcall_ret_15),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_15))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_15))));
  for (i = 0; i < len; i++) {
    int i_index_16;
    (i_index_16 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&dst)),((unsigned long )(&dst[i_index_16]))));
    int i_index_17;
    (i_index_17 = len - i - 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&src)),((unsigned long )(&src[i_index_17]))));
    dst[i_index_16] = src[i_index_17];
  }
}
//Returns TRUE if the integer represented by string left is GREATER THAN the integer represented by right

int greaterThan(char *left,int left_len,char *right,int right_len)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&right)),UL_Ret_get_from_stack_i_Arg(1));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&left)),UL_Ret_get_from_stack_i_Arg(0));
  int i;
//The longer string is greater
  if (left_len != right_len) {
    return left_len > right_len;
  }
//At this point we know the strings are the same length
//Go from most significant digit to least, comparing along the way.
  for (i = 0; i < left_len; i++) {
    int i_index_18;
    (i_index_18 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&left)),((unsigned long )(&left[i_index_18]))));
    int i_index_19;
    (i_index_19 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&right)),((unsigned long )(&right[i_index_19]))));
    if (left[i_index_18] != right[i_index_19]) {
      int i_index_20;
      (i_index_20 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&left)),((unsigned long )(&left[i_index_20]))));
      int i_index_21;
      (i_index_21 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&right)),((unsigned long )(&right[i_index_21]))));
      return left[i_index_20] > right[i_index_21];
    }
  }
//If the code gets here, the strings are equal, so return FALSE
  return 0;
}

int do_weakness(char *k,char **argv,int i)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&argv)),UL_Ret_get_from_stack_i_Arg(1));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&k)),UL_Ret_get_from_stack_i_Arg(0));
  if (i == 0) {
//STONESOUP:INTERACTION_POINT	//STONESOUP:CROSSOVER_POINT	//STONESOUP:TRIGGER_POINT	//STONESOUP:SOURCE_TAINT:COMMAND_LINE
    int i_index_22;
    (i_index_22 = 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&argv)),((unsigned long )(&argv[i_index_22]))));
    char *__Pb__c__Pe___libcall_ret_23;
    ((__Pb__c__Pe___libcall_ret_23 = strcpy(k,__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(argv[i_index_22],((unsigned long long )(&argv[i_index_22]))))) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__c__Pe___libcall_ret_23)),((unsigned long long )__Pb__c__Pe___libcall_ret_23),((unsigned long )(sizeof(__Pb__c__Pe___libcall_ret_23))))) , __Pb__c__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__c__Pe___libcall_ret_23))));
  }
  return 27;
}
static char* global_var24_0_0 = "This software (or technical data) was produced for the U. S.    Government under contract 2009-0917826-016 and is subject to    the Rights in Data-General Clause 52.227-14. Alt. IV (DEC 2007).    (c) Copyright 2012 The MITRE Corporation. All Rights Reserved.";
static char* global_var26_248_10 = "Error: Expected one argument";
static int global_var28_254_10 = 0;
static char* global_var30_256_10 = "Error: Unable to calloc %d characters";
static char* global_var33_270_10 = "Error: Input length was greater than 1,000,000 characters";
static char* global_var35_277_10 = "Error: Input did not contain only numbers [0-9]";
static int global_var38_285_16 = 0;
static char* global_var40_288_11 = "Error: Unable to calloc %d characters";
static char* global_var42_292_10 = "2";
static char* global_var45_293_10 = "%s";
static int global_var47_296_12 = 0;
static int global_var50_304_16 = 0;
static char* global_var52_307_11 = "Error: Unable to calloc %d characters";
static char* global_var54_311_10 = "1";
static char* global_var57_312_10 = "%s";
static int global_var60_323_15 = 0;
static char* global_var62_326_10 = "Error: Unable to calloc %d characters";
static int global_var65_330_24 = 0;
static char* global_var67_334_10 = "Error: Unable to calloc %d characters";
static int global_var70_338_17 = 0;
static char* global_var72_343_10 = "Error: Unable to calloc %d characters";
static int global_var76_348_16 = 0;
static char* global_var78_354_10 = "Error: Unable to calloc %d characters";
static int global_var81_358_23 = 0;
static char* global_var83_365_10 = "Error: Unable to calloc %d characters";
static int global_var86_369_32 = 0;
static char* global_var88_377_10 = "Error: Unable to calloc %d characters";
static char* global_var90_382_14 = "1";
static char* global_var95_392_11 = "%s%s%s";
static char* global_var97_394_11 = "%s%c%s";
static char* global_var100_399_11 = "%s%s";
static char* global_var102_401_11 = "%s%s";

int main(int argc,char **argv)
{
  v_Ret_execAtFirst();
  v_Ret_create_dummy_entry_UL_Arg(((unsigned long long )(&argv)));
  int index_argc;
  for (index_argc = 0; index_argc < argc; ++index_argc) 
    v_Ret_create_dummy_entry_UL_Arg(((unsigned long long )(&argv[index_argc])));
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&data_rights_legend,__Pb__c__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((char *)global_var24_0_0),((unsigned long long )(&global_var24_0_0)),((unsigned long )(sizeof(global_var24_0_0))),((unsigned long long )50)));
  char *k;
  int k_len;
  int isLengthOdd;
  int half_k_len;
  char *k_left;
  char *k_left_plus_one;
  char *k_middle;
  char *k_right;
  char *k_left_reverse;
  char *k_left_plus_one_reverse;
  int isLeftReverseBigger;
  if (argc != 2) {
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var26_248_10),((unsigned long long )(&global_var26_248_10)),((unsigned long )(sizeof(global_var26_248_10))),((unsigned long long )50))));
    return -1;
  }
//STONESOUP:DATA_TYPE:HEAP_POINTER
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&k,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_malloc_overload_Ul_Arg(((unsigned long )(sizeof(char ) * 300)))));
  if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(k,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var28_254_10),((unsigned long long )(&global_var28_254_10)),((unsigned long )(sizeof(global_var28_254_10))),((unsigned long long )50))))) {
    int i_index_32;
    (i_index_32 = 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&argv)),((unsigned long )(&argv[i_index_32]))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var30_256_10),((unsigned long long )(&global_var30_256_10)),((unsigned long )(sizeof(global_var30_256_10))),((unsigned long long )50))),strlen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(argv[i_index_32],((unsigned long long )(&argv[i_index_32]))))) + 1);
    return -1;
  }
  int i;
//STONESOUP:CONTROL_FLOW:LOOP_COMPLEXITY_TEST	//STONESOUP:DATA_FLOW:PASS_BY_REFERENCE
  for (i = 0; i < ((((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&argv)))) , do_weakness(k,argv,i)) , UL_Ret_pop_from_stack_i_Arg(2))); i += 2) {
    i -= 1;
  }
  k_len = (strlen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))))));
  if (k_len > 300) {
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var33_270_10),((unsigned long long )(&global_var33_270_10)),((unsigned long )(sizeof(global_var33_270_10))),((unsigned long long )50))));
    return -1;
  }
  if (!(((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k))) , isAllInts(k)) , UL_Ret_pop_from_stack_i_Arg(1)))) {
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var35_277_10),((unsigned long long )(&global_var35_277_10)),((unsigned long )(sizeof(global_var35_277_10))),((unsigned long long )50))));
    exit(-1);
  }
//If the string is all nines, the next highest palindrome is always k+2
  if (((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k))) , isAllNines(k)) , UL_Ret_pop_from_stack_i_Arg(1))) {
    void *__Pb__v__Pe___libcall_ret_37;
    char *result;
    __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&result,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg((((__Pb__v__Pe___libcall_ret_37 = calloc((k_len + 2),sizeof(char )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_37)),((unsigned long long )__Pb__v__Pe___libcall_ret_37),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_37))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_37)))))));
    if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(result,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var38_285_16),((unsigned long long )(&global_var38_285_16)),((unsigned long )(sizeof(global_var38_285_16))),((unsigned long long )50))))) {
      v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
      printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var40_288_11),((unsigned long long )(&global_var40_288_11)),((unsigned long )(sizeof(global_var40_288_11))),((unsigned long long )50))),k_len + 2);
      exit(-1);
    }
    struct __Pb__c__Pe___Type argvar_44;
    ((((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k))) , ((argvar_44 = __Pb__c__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((char *)global_var42_292_10),((unsigned long long )(&global_var42_292_10)),((unsigned long )(sizeof(global_var42_292_10))),((unsigned long long )50)) , v_Ret_push_to_stack_UL_Arg(argvar_44 . addr)))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&result)))) , add(k,argvar_44 . ptr,result,k_len + 1)) , UL_Ret_pop_from_stack_i_Arg(3));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var45_293_10),((unsigned long long )(&global_var45_293_10)),((unsigned long )(sizeof(global_var45_293_10))),((unsigned long long )50))),result);
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(result,((unsigned long long )(&result))));
    __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&result,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var47_296_12),((unsigned long long )(&global_var47_296_12)),((unsigned long )(sizeof(global_var47_296_12))),((unsigned long long )50))));
    return 0;
  }
//If the string is not nine, but is a single digit (ie 0-8), the next highest palindrom is k+1
  if (k_len == 1) {
    void *__Pb__v__Pe___libcall_ret_49;
    char *result;
    __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&result,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg((((__Pb__v__Pe___libcall_ret_49 = calloc((k_len + 2),sizeof(char )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_49)),((unsigned long long )__Pb__v__Pe___libcall_ret_49),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_49))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_49)))))));
    if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(result,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var50_304_16),((unsigned long long )(&global_var50_304_16)),((unsigned long )(sizeof(global_var50_304_16))),((unsigned long long )50))))) {
      v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
      printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var52_307_11),((unsigned long long )(&global_var52_307_11)),((unsigned long )(sizeof(global_var52_307_11))),((unsigned long long )50))),k_len + 2);
      exit(-1);
    }
    struct __Pb__c__Pe___Type argvar_56;
    ((((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k))) , ((argvar_56 = __Pb__c__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((char *)global_var54_311_10),((unsigned long long )(&global_var54_311_10)),((unsigned long )(sizeof(global_var54_311_10))),((unsigned long long )50)) , v_Ret_push_to_stack_UL_Arg(argvar_56 . addr)))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&result)))) , add(k,argvar_56 . ptr,result,k_len + 1)) , UL_Ret_pop_from_stack_i_Arg(3));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var57_312_10),((unsigned long long )(&global_var57_312_10)),((unsigned long )(sizeof(global_var57_312_10))),((unsigned long long )50))),result);
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(result,((unsigned long long )(&result))));
    return 0;
  }
//Compute/initialize all variables that I might use
  isLengthOdd = k_len % 2 == 1;
  half_k_len = (k_len - isLengthOdd) / 2;
  void *__Pb__v__Pe___libcall_ret_59;
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&k_left,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg((((__Pb__v__Pe___libcall_ret_59 = calloc((half_k_len + 1),sizeof(char )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_59)),((unsigned long long )__Pb__v__Pe___libcall_ret_59),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_59))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_59)))))));
  if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(k_left,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var60_323_15),((unsigned long long )(&global_var60_323_15)),((unsigned long )(sizeof(global_var60_323_15))),((unsigned long long )50))))) {
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var62_326_10),((unsigned long long )(&global_var62_326_10)),((unsigned long )(sizeof(global_var62_326_10))),((unsigned long long )50))),half_k_len + 1);
    exit(-1);
  }
  void *__Pb__v__Pe___libcall_ret_64;
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&k_left_plus_one,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg((((__Pb__v__Pe___libcall_ret_64 = calloc((half_k_len + 2),sizeof(char )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_64)),((unsigned long long )__Pb__v__Pe___libcall_ret_64),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_64))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_64)))))));
  if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(k_left_plus_one,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var65_330_24),((unsigned long long )(&global_var65_330_24)),((unsigned long )(sizeof(global_var65_330_24))),((unsigned long long )50))))) {
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left,((unsigned long long )(&k_left))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var67_334_10),((unsigned long long )(&global_var67_334_10)),((unsigned long )(sizeof(global_var67_334_10))),((unsigned long long )50))),half_k_len + 2);
    exit(-1);
  }
  void *__Pb__v__Pe___libcall_ret_69;
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&k_middle,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg((((__Pb__v__Pe___libcall_ret_69 = calloc(2,sizeof(char )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_69)),((unsigned long long )__Pb__v__Pe___libcall_ret_69),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_69))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_69)))))));
  if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(k_middle,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var70_338_17),((unsigned long long )(&global_var70_338_17)),((unsigned long )(sizeof(global_var70_338_17))),((unsigned long long )50))))) {
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left,((unsigned long long )(&k_left))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_plus_one,((unsigned long long )(&k_left_plus_one))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var72_343_10),((unsigned long long )(&global_var72_343_10)),((unsigned long )(sizeof(global_var72_343_10))),((unsigned long long )50))),2);
    exit(-1);
  }
  int i_index_74;
  (i_index_74 = 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&k_middle)),((unsigned long )(&k_middle[i_index_74]))));
  k_middle[i_index_74] = '\0';
  void *__Pb__v__Pe___libcall_ret_75;
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&k_right,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg((((__Pb__v__Pe___libcall_ret_75 = calloc((half_k_len + 1),sizeof(char )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_75)),((unsigned long long )__Pb__v__Pe___libcall_ret_75),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_75))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_75)))))));
  if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(k_right,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var76_348_16),((unsigned long long )(&global_var76_348_16)),((unsigned long )(sizeof(global_var76_348_16))),((unsigned long long )50))))) {
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left,((unsigned long long )(&k_left))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_plus_one,((unsigned long long )(&k_left_plus_one))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_middle,((unsigned long long )(&k_middle))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var78_354_10),((unsigned long long )(&global_var78_354_10)),((unsigned long )(sizeof(global_var78_354_10))),((unsigned long long )50))),half_k_len + 1);
    exit(-1);
  }
  void *__Pb__v__Pe___libcall_ret_80;
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&k_left_reverse,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg((((__Pb__v__Pe___libcall_ret_80 = calloc((half_k_len + 1),sizeof(char )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_80)),((unsigned long long )__Pb__v__Pe___libcall_ret_80),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_80))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_80)))))));
  if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(k_left_reverse,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var81_358_23),((unsigned long long )(&global_var81_358_23)),((unsigned long )(sizeof(global_var81_358_23))),((unsigned long long )50))))) {
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left,((unsigned long long )(&k_left))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_plus_one,((unsigned long long )(&k_left_plus_one))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_middle,((unsigned long long )(&k_middle))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_right,((unsigned long long )(&k_right))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var83_365_10),((unsigned long long )(&global_var83_365_10)),((unsigned long )(sizeof(global_var83_365_10))),((unsigned long long )50))),half_k_len + 1);
    exit(-1);
  }
  void *__Pb__v__Pe___libcall_ret_85;
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&k_left_plus_one_reverse,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg((((__Pb__v__Pe___libcall_ret_85 = calloc((half_k_len + 2),sizeof(char )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_85)),((unsigned long long )__Pb__v__Pe___libcall_ret_85),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_85))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_85)))))));
  if (i_Ret_Equality___Pb__c__Pe___Arg___Pb__c__Pe___Type_Arg(k_left_plus_one_reverse,__Pb__c__Pe___Type_Ret_Cast___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__v__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((void *)global_var86_369_32),((unsigned long long )(&global_var86_369_32)),((unsigned long )(sizeof(global_var86_369_32))),((unsigned long long )50))))) {
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left,((unsigned long long )(&k_left))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_plus_one,((unsigned long long )(&k_left_plus_one))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_middle,((unsigned long long )(&k_middle))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_right,((unsigned long long )(&k_right))));
    v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_reverse,((unsigned long long )(&k_left_reverse))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var88_377_10),((unsigned long long )(&global_var88_377_10)),((unsigned long )(sizeof(global_var88_377_10))),((unsigned long long )50))),half_k_len + 2);
    exit(-1);
  }
  (((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_left)))) , substr(k,0,half_k_len,k_left)) , UL_Ret_pop_from_stack_i_Arg(2));
  struct __Pb__c__Pe___Type argvar_92;
  ((((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_left))) , ((argvar_92 = __Pb__c__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((char *)global_var90_382_14),((unsigned long long )(&global_var90_382_14)),((unsigned long )(sizeof(global_var90_382_14))),((unsigned long long )50)) , v_Ret_push_to_stack_UL_Arg(argvar_92 . addr)))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_left_plus_one)))) , add(k_left,argvar_92 . ptr,k_left_plus_one,half_k_len + 1)) , UL_Ret_pop_from_stack_i_Arg(3));
  int i_index_93;
  (i_index_93 = 0 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&k_middle)),((unsigned long )(&k_middle[i_index_93]))));
  int i_index_94;
  (i_index_94 = half_k_len , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&k)),((unsigned long )(&k[i_index_94]))));
  k_middle[i_index_93] = ((isLengthOdd?k[i_index_94] : '\0'));
  (((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_right)))) , substr(k,half_k_len + isLengthOdd,half_k_len,k_right)) , UL_Ret_pop_from_stack_i_Arg(2));
  (((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_left))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_left_reverse)))) , reverse(k_left,k_left_reverse,half_k_len)) , UL_Ret_pop_from_stack_i_Arg(2));
  (((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_left_plus_one))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_left_plus_one_reverse)))) , reverse(k_left_plus_one,k_left_plus_one_reverse,(strlen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_plus_one,((unsigned long long )(&k_left_plus_one)))))))) , UL_Ret_pop_from_stack_i_Arg(2));
  isLeftReverseBigger = ((((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_left_reverse))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&k_right)))) , greaterThan(k_left_reverse,half_k_len,k_right,half_k_len)) , UL_Ret_pop_from_stack_i_Arg(2)));
  if (isLengthOdd) {
    if (isLeftReverseBigger) {
      printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var95_392_11),((unsigned long long )(&global_var95_392_11)),((unsigned long )(sizeof(global_var95_392_11))),((unsigned long long )50))),k_left,k_middle,k_left_reverse);
    }
    else {
      int i_index_99;
      (i_index_99 = 0 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&k_middle)),((unsigned long )(&k_middle[i_index_99]))));
      printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var97_394_11),((unsigned long long )(&global_var97_394_11)),((unsigned long )(sizeof(global_var97_394_11))),((unsigned long long )50))),k_left,k_middle[i_index_99] + 1,k_left_reverse);
    }
  }
  else 
//Length is even
{
    if (isLeftReverseBigger) {
      printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var100_399_11),((unsigned long long )(&global_var100_399_11)),((unsigned long )(sizeof(global_var100_399_11))),((unsigned long long )50))),k_left,k_left_reverse);
    }
    else {
      printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var102_401_11),((unsigned long long )(&global_var102_401_11)),((unsigned long )(sizeof(global_var102_401_11))),((unsigned long long )50))),k_left_plus_one,k_left_plus_one_reverse);
    }
  }
//Free allocated memory
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k,((unsigned long long )(&k))));
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left,((unsigned long long )(&k_left))));
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_plus_one,((unsigned long long )(&k_left_plus_one))));
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_middle,((unsigned long long )(&k_middle))));
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_right,((unsigned long long )(&k_right))));
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_reverse,((unsigned long long )(&k_left_reverse))));
  v_Ret_free_overload___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg(k_left_plus_one_reverse,((unsigned long long )(&k_left_plus_one_reverse))));
  v_Ret_execAtLast();
  return 0;
}
