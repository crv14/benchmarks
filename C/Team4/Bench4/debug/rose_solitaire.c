unsigned long long UL_Ret_get_from_stack_i_Arg(int input1);
void v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(unsigned long long input1,unsigned long long input2);

struct __Pb__Cc__Pe___Type 
{
  const char *ptr;
  unsigned long long addr;
}
;
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(unsigned long long input1,unsigned long long input2,unsigned long input3,unsigned long long input4);

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_create_struct___Pb__Cc__Pe___Arg_UL_Arg(const char *input1,unsigned long long input2)
{
  struct __Pb__Cc__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(const char *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return __Pb__Cc__Pe___Type_Ret_create_struct___Pb__Cc__Pe___Arg_UL_Arg(input1,input2);
}
void v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(unsigned long long input1,unsigned long input2);

static const char *__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(struct __Pb__Cc__Pe___Type input1)
{
  return input1 . ptr;
}
void v_Ret_push_to_stack_UL_Arg(unsigned long long input1);
unsigned long long UL_Ret_pop_from_stack_i_Arg(int input1);
void v_Ret_array_bound_check_Ui_Arg_Ui_Arg(unsigned int input1,unsigned int input2);

struct __Pb__v__Pe___Type 
{
  void *ptr;
  unsigned long long addr;
}
;

struct __Pb__Cv__Pe___Type 
{
  const void *ptr;
  unsigned long long addr;
}
;

static void *__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(struct __Pb__v__Pe___Type input1)
{
  return input1 . ptr;
}

static const void *__Pb__Cv__Pe___Ret_return_pointer___Pb__Cv__Pe___Type_Arg(struct __Pb__Cv__Pe___Type input1)
{
  return input1 . ptr;
}
void v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(unsigned long long input1,unsigned long long input2,unsigned long input3);

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(unsigned long long input1)
{
  struct __Pb__v__Pe___Type output;
  output . ptr =  *((void **)input1);
  output . addr = input1;
  return output;
}

struct __Pb____Pb__CUs__Pe____Pe___Type 
{
  const unsigned short **ptr;
  unsigned long long addr;
}
;

static struct __Pb____Pb__CUs__Pe____Pe___Type __Pb____Pb__CUs__Pe____Pe___Type_Ret_create_struct_from_addr_UL_Arg(unsigned long long input1)
{
  struct __Pb____Pb__CUs__Pe____Pe___Type output;
  output . ptr =  *((const unsigned short ***)input1);
  output . addr = input1;
  return output;
}
void v_Ret_check_entry_UL_Arg_UL_Arg(unsigned long long input1,unsigned long long input2);
void v_Ret_null_check_UL_Arg(unsigned long long input1);

static const unsigned short **__Pb____Pb__CUs__Pe____Pe___Ret_deref_check___Pb____Pb__CUs__Pe____Pe___Arg_UL_Arg(const unsigned short **input1,unsigned long long input2)
{
  v_Ret_check_entry_UL_Arg_UL_Arg(((unsigned long long )input1),input2);
  v_Ret_null_check_UL_Arg(((unsigned long long )input1));
  return input1;
}

static const unsigned short **__Pb____Pb__CUs__Pe____Pe___Ret_deref_check_with_str___Pb____Pb__CUs__Pe____Pe___Type_Arg(struct __Pb____Pb__CUs__Pe____Pe___Type input1)
{
  return __Pb____Pb__CUs__Pe____Pe___Ret_deref_check___Pb____Pb__CUs__Pe____Pe___Arg_UL_Arg(input1 . ptr,((unsigned long long )input1 . addr));
}

struct __Pb__c__Pe___Type 
{
  char *ptr;
  unsigned long long addr;
}
;

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(char *input1,unsigned long long input2)
{
  struct __Pb__c__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(char *input1,unsigned long long input2,unsigned long input3,unsigned long long input4)
{
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(input2,((unsigned long long )input1),input3,input4);
  return __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(input1,input2);
}

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(char **input1,struct __Pb__c__Pe___Type input2)
{
   *input1 = input2 . ptr;
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )input1),((unsigned long long )input2 . addr));
  return __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(input2 . ptr,((unsigned long long )input1));
}

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_Cast__Ab_c_index_20_Ae__Arg_UL_Arg(char input1[20],unsigned long long input2)
{
  struct __Pb__v__Pe___Type output;
  output . ptr = ((void *)input1);
  output . addr = input2;
  return output;
}

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_create_struct_from_addr_UL_Arg(unsigned long long input1)
{
  struct __Pb__c__Pe___Type output;
  output . ptr =  *((char **)input1);
  output . addr = input1;
  return output;
}

static struct __Pb__Cc__Pe___Type __Pb__Cc__Pe___Type_Ret_Cast__Ab_c_index_20_Ae__Arg_UL_Arg(char input1[20],unsigned long long input2)
{
  struct __Pb__Cc__Pe___Type output;
  output . ptr = ((const char *)input1);
  output . addr = input2;
  return output;
}

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Arg_UL_Arg(char **input1,char *input2,unsigned long long input3)
{
   *input1 = input2;
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )input1),input3);
  return __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(input2,((unsigned long long )input1));
}

static char *__Pb__c__Pe___Ret_deref_check___Pb__c__Pe___Arg_UL_Arg(char *input1,unsigned long long input2)
{
  v_Ret_check_entry_UL_Arg_UL_Arg(((unsigned long long )input1),input2);
  v_Ret_null_check_UL_Arg(((unsigned long long )input1));
  return input1;
}

static struct __Pb__c__Pe___Type __Pb__c__Pe___Type_Ret_Increment___Pb____Pb__c__Pe____Pe___Arg(char **input1)
{
  ++ *input1;
  struct __Pb__c__Pe___Type output;
  output . ptr =  *input1;
  output . addr = ((unsigned long long )input1);
  return output;
}

static char *__Pb__c__Pe___Ret_deref_check_with_str___Pb__c__Pe___Type_Arg(struct __Pb__c__Pe___Type input1)
{
  return __Pb__c__Pe___Ret_deref_check___Pb__c__Pe___Arg_UL_Arg(input1 . ptr,((unsigned long long )input1 . addr));
}

static char *__Pb__c__Pe___Ret_return_pointer___Pb__c__Pe___Type_Arg(struct __Pb__c__Pe___Type input1)
{
  return input1 . ptr;
}
void v_Ret_create_dummy_entry_UL_Arg(unsigned long long input1);
void v_Ret_execAtFirst();
void v_Ret_execAtLast();
static char *data_rights_legend;
/********************************************************************
 ** This software (or technical data) was produced for the U. S.
 ** Government under contract 2009-0917826-016 and is subject to 
 ** the Rights in Data-General Clause 52.227-14. Alt. IV (DEC 2007).
 ** 
 ** (c) Copyright 2012 The MITRE Corporation. All Rights Reserved.
 ********************************************************************/
/*******************************************
**
**
** 
** Date: 8/8/2011
**
** Base Test Program -- solitare.c
**
** <base test program description, note that this description
** should not talk about things like source taint and any other
** variant categories as those details will change>
**
** Variant Test Case Program
**
 ** The program does this:
 ** 1) Reads in up to 25 bytes from a key file, and converts those characters into a 32-bit unsigned integer
 ** 2) Using that 32-bit unsigned integer as a seed, creates a shuffled deck of 54 cards (with jokers)
 ** 3) Read in a message to be encoded
 ** 4) Strip the message of all non-alphabet-letters
 ** 5) Read a new keystream value from the deck by
 ** 	a) Moving the first joker one spot down the deck
 ** 	b) Moving the second joker two spots down the deck
 ** 	c) Cutting the deck into three parts, and reversing the order of them relative to each other, delimited by the jokers
 ** 	d) Cutting the deck by as many cards as the value of the bottom card
 ** 	e) The next keystream value is the nth card from the top, where n is the card at the top of the deck
 ** 	f) The deck stays as it ends up
 ** 6) Encrypt the first character by adding the keystream value to it, mod 26
 ** 7) Perform steps 5-6 until done
 **
 ** Decryption works similarly.
 **
 ** See http://en.wikipedia.org/wiki/Solitaire_(cipher)
 **
 **
** STONESOUP Weakness Class: Buffer overflows/underflows/out of bounds/accesses/memory safety errors
** CWE ID: CWE-170
** Variant Spreadsheet Rev #: 2
** Variant Spreadsheet ID: 986
**
** Variant Features:
**		SOURCE_TAINT:STDIN
**		DATA_TYPE:ARRAY_LENGTH_VARIABLE
**		CONTROL_FLOW:END_CONDITION_CONTROLLED_LOOP
**		DATA_FLOW:ARRAY_INDEX_ARRAY_CONTENT_VALUE
**
** Initial Testing: (x means yes, - means no)
**   Tested in MS Windows XP 32bit        x
**   Tested in MS Windows 7  64bit        -
**   Tested in Ubuntu10_10 Linux 32bit    -
**   Tested in Ubuntu10_10 Linux 64bit    -
**
** Workflow:
**   Created:8/8/2011
**   1st Vett: <programmer> on <date>
**   2nd Vett: <peer> on <date>
**   3rd Vett: <teamleader> on <date>
**   4th Vett: Fortify Issues Fixed on <date>
**   5th Vett: Tested in Harness on <date>
**
**
** I/0 Pairs:
**   Good: 1st Set:
**         2nd Set:
**         3rd Set:
**         4th Set:
**         5th Set:
**    Bad: 1st Set:
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "random_lw.h"
#define KEY_LENGTH 25
#define DECK_SIZE 54
#define ALPHA_SIZE 26
#define FIRST_JOKER 53
#define SECOND_JOKER 54
#define MAX_MESSAGE_LENGTH 20
typedef char Card;
static char* global_var0_108_10 = "%d ";
static char* global_var3_110_9 = "\n";

void print_deck(Card *deck)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  int i;
  for (i = 0; i < 54; i++) {
    int i_index_2;
    (i_index_2 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_2]))));
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var0_108_10),((unsigned long long )(&global_var0_108_10)),((unsigned long )(sizeof(global_var0_108_10))),((unsigned long long )50))),deck[i_index_2]);
  }
  printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var3_110_9),((unsigned long long )(&global_var3_110_9)),((unsigned long )(sizeof(global_var3_110_9))),((unsigned long long )50))));
}
//In-place Fischer-Yates shuffle

void shuffle(Card *deck,unsigned int seed)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  int cards_left = 54;
  int next_index;
  int cur_index;
  Card temp;
  srand1(seed);
  while(1){
    if (!(cards_left != 0)) 
      break; 
    next_index = (54 - rand1() % cards_left - 1);
    cur_index = 54 - cards_left;
    int i_index_5;
    (i_index_5 = cur_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_5]))));
    temp = deck[i_index_5];
    int i_index_6;
    (i_index_6 = cur_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_6]))));
    int i_index_7;
    (i_index_7 = next_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_7]))));
    deck[i_index_6] = deck[i_index_7];
    int i_index_8;
    (i_index_8 = next_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_8]))));
    deck[i_index_8] = temp;
    cards_left--;
  }
}
//turn the key into a 32-bit unsigned int via questionably required

unsigned int seedify(char *key)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&key)),UL_Ret_get_from_stack_i_Arg(0));
  int i;
  unsigned int seed = 0;
  for (i = 0; i < 25; i++) {
    int i_index_9;
    (i_index_9 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&key)),((unsigned long )(&key[i_index_9]))));
    seed += ((unsigned int )key[i_index_9]) << i % 32;
  }
  return seed;
}
//randomly initializes the deck

void init_deck(Card *deck,char *key)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&key)),UL_Ret_get_from_stack_i_Arg(1));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  int i;
  for (i = 0; i < 54; i++) {
    int i_index_10;
    (i_index_10 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_10]))));
    deck[i_index_10] = ((Card )(i + 1));
  }
  ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , shuffle(deck,(((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&key))) , seedify(key)) , UL_Ret_pop_from_stack_i_Arg(1))))) , UL_Ret_pop_from_stack_i_Arg(1));
}

int indexof(Card *deck,Card c)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  int i;
  for (i = 0; i < 54; i++) {
    int i_index_11;
    (i_index_11 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_11]))));
    if (deck[i_index_11] == c) {
      return i;
    }
  }
  return -1;
}

void joker_shift(Card *deck)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  int fji = ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , indexof(deck,53)) , UL_Ret_pop_from_stack_i_Arg(1));
  if (fji < 54 - 1 && fji >= 0) {
    int i_index_12;
    (i_index_12 = fji , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_12]))));
    int i_index_13;
    (i_index_13 = fji + 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_13]))));
    deck[i_index_12] = deck[i_index_13];
    int i_index_14;
    (i_index_14 = fji + 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_14]))));
    deck[i_index_14] = 53;
    fji++;
  }
  int sji = ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , indexof(deck,54)) , UL_Ret_pop_from_stack_i_Arg(1));
  if (sji < 54 - 1 && sji >= 0) {
    int i_index_15;
    (i_index_15 = sji , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_15]))));
    int i_index_16;
    (i_index_16 = sji + 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_16]))));
    deck[i_index_15] = deck[i_index_16];
    int i_index_17;
    (i_index_17 = sji + 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_17]))));
    deck[i_index_17] = 54;
    sji++;
  }
  if (sji < 54 - 1 && sji >= 0) {
    int i_index_18;
    (i_index_18 = sji , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_18]))));
    int i_index_19;
    (i_index_19 = sji + 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_19]))));
    deck[i_index_18] = deck[i_index_19];
    int i_index_20;
    (i_index_20 = sji + 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_20]))));
    deck[i_index_20] = 54;
    sji++;
  }
}
static char* global_var21_184_10 = "Something bad happened... jokers are not there right.\n";

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_Cast___Pb__Cardc__typedef_declaration__Pe___Arg_UL_Arg(Card *input1,unsigned long long input2)
{
  struct __Pb__v__Pe___Type output;
  output . ptr = ((void *)input1);
  output . addr = input2;
  return output;
}

static struct __Pb__Cv__Pe___Type __Pb__Cv__Pe___Type_Ret_Cast__Ab_Cardc__typedef_declaration_index_54_Ae__Arg_UL_Arg(Card input1[54],unsigned long long input2)
{
  struct __Pb__Cv__Pe___Type output;
  output . ptr = ((const void *)input1);
  output . addr = input2;
  return output;
}

void triple_cut(Card *deck)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  Card helper_deck[54];
  int fji = ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , indexof(deck,53)) , UL_Ret_pop_from_stack_i_Arg(1));
  int sji = ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , indexof(deck,54)) , UL_Ret_pop_from_stack_i_Arg(1));
  if (fji > 54 || fji < 0 || sji > 54 || sji < 0) {
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var21_184_10),((unsigned long long )(&global_var21_184_10)),((unsigned long )(sizeof(global_var21_184_10))),((unsigned long long )50))));
    return ;
  }
  int top_joker_index;
  int bottom_joker_index;
  if (fji < sji) {
    top_joker_index = fji;
    bottom_joker_index = sji;
  }
  else {
    top_joker_index = sji;
    bottom_joker_index = fji;
  }
  int write_index = 0;
  int read_index = bottom_joker_index + 1;
  while(1){
    if (!(read_index < 54 && write_index < 54)) 
      break; 
    int i_index_23;
    (i_index_23 = write_index , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(helper_deck) / sizeof(char ),((unsigned int )i_index_23)));
    int i_index_24;
    (i_index_24 = read_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_24]))));
    helper_deck[i_index_23] = deck[i_index_24];
    read_index++;
    write_index++;
  }
  read_index = top_joker_index;
  while(1){
    if (!(read_index <= bottom_joker_index && write_index < 54)) 
      break; 
    int i_index_25;
    (i_index_25 = write_index , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(helper_deck) / sizeof(char ),((unsigned int )i_index_25)));
    int i_index_26;
    (i_index_26 = read_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_26]))));
    helper_deck[i_index_25] = deck[i_index_26];
    read_index++;
    write_index++;
  }
  read_index = 0;
  while(1){
    if (!(read_index < top_joker_index && write_index < 54)) 
      break; 
    int i_index_27;
    (i_index_27 = write_index , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(helper_deck) / sizeof(char ),((unsigned int )i_index_27)));
    int i_index_28;
    (i_index_28 = read_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_28]))));
    helper_deck[i_index_27] = deck[i_index_28];
    read_index++;
    write_index++;
  }
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&helper_deck)),((unsigned long long )helper_deck),((unsigned long )(sizeof(helper_deck))),((unsigned long long )50));
  void *__Pb__v__Pe___libcall_ret_29;
  ((__Pb__v__Pe___libcall_ret_29 = memcpy(__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__Cardc__typedef_declaration__Pe___Arg_UL_Arg(deck,((unsigned long long )(&deck)))),__Pb__Cv__Pe___Ret_return_pointer___Pb__Cv__Pe___Type_Arg(__Pb__Cv__Pe___Type_Ret_Cast__Ab_Cardc__typedef_declaration_index_54_Ae__Arg_UL_Arg(helper_deck,((unsigned long long )(&helper_deck)))),54 * sizeof(Card )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_29)),((unsigned long long )__Pb__v__Pe___libcall_ret_29),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_29))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_29))));
}

void bottom_value_cut(Card *deck)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  Card helper_deck[54];
  int i_index_30;
  (i_index_30 = 54 - 1 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_30]))));
  int bottom_value = (int )deck[i_index_30];
  if (bottom_value >= 54) {
    bottom_value = 54 - 1;
  }
  int read_index = bottom_value;
  int write_index = 0;
  while(1){
    if (!(read_index < 54 && write_index < 54)) 
      break; 
    int i_index_31;
    (i_index_31 = write_index , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(helper_deck) / sizeof(char ),((unsigned int )i_index_31)));
    int i_index_32;
    (i_index_32 = read_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_32]))));
    helper_deck[i_index_31] = deck[i_index_32];
    read_index++;
    write_index++;
  }
  read_index = 0;
  while(1){
    if (!(read_index < bottom_value && write_index < 54)) 
      break; 
    int i_index_33;
    (i_index_33 = write_index , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(helper_deck) / sizeof(char ),((unsigned int )i_index_33)));
    int i_index_34;
    (i_index_34 = read_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_34]))));
    helper_deck[i_index_33] = deck[i_index_34];
    read_index++;
    write_index++;
  }
  read_index = 54 - 1;
  while(1){
    if (!(read_index < 54 && write_index < 54)) 
      break; 
    int i_index_35;
    (i_index_35 = write_index , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(helper_deck) / sizeof(char ),((unsigned int )i_index_35)));
    int i_index_36;
    (i_index_36 = read_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_36]))));
    helper_deck[i_index_35] = deck[i_index_36];
    read_index++;
    write_index++;
  }
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&helper_deck)),((unsigned long long )helper_deck),((unsigned long )(sizeof(helper_deck))),((unsigned long long )50));
  void *__Pb__v__Pe___libcall_ret_37;
  ((__Pb__v__Pe___libcall_ret_37 = memcpy(__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast___Pb__Cardc__typedef_declaration__Pe___Arg_UL_Arg(deck,((unsigned long long )(&deck)))),__Pb__Cv__Pe___Ret_return_pointer___Pb__Cv__Pe___Type_Arg(__Pb__Cv__Pe___Type_Ret_Cast__Ab_Cardc__typedef_declaration_index_54_Ae__Arg_UL_Arg(helper_deck,((unsigned long long )(&helper_deck)))),54 * sizeof(Card )) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_37)),((unsigned long long )__Pb__v__Pe___libcall_ret_37),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_37))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_37))));
}
//Does all the algorithmy stuff described in the header

int next_keystream_value(Card *deck)
{
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , joker_shift(deck)) , UL_Ret_pop_from_stack_i_Arg(1));
  ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , triple_cut(deck)) , UL_Ret_pop_from_stack_i_Arg(1));
  ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , bottom_value_cut(deck)) , UL_Ret_pop_from_stack_i_Arg(1));
  int i_index_38;
  (i_index_38 = 0 , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_38]))));
  int return_index = deck[i_index_38];
  if (return_index >= 54) {
    return_index = 54 - 1;
  }
  int i_index_39;
  (i_index_39 = return_index , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&deck)),((unsigned long )(&deck[i_index_39]))));
  return deck[i_index_39];
}

char *encrypt(Card *deck,char *message,char *encrypted)
{
//message index
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&encrypted)),UL_Ret_get_from_stack_i_Arg(2));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&message)),UL_Ret_get_from_stack_i_Arg(1));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  int i = 0;
//encrypted index
  int j = 0;
  char c;
  while(1){
    int i_index_40;
    (i_index_40 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&message)),((unsigned long )(&message[i_index_40]))));
    if (!(message[i_index_40] != '\0' && i < 20)) 
      break; 
    int i_index_41;
    (i_index_41 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&message)),((unsigned long )(&message[i_index_41]))));
    c = message[i_index_41];
    const unsigned short **__Pb____Pb__CUs__Pe____Pe___libcall_ret_42;
    int i_index_43;
    (i_index_43 = ((int )c) , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&( *__Pb____Pb__CUs__Pe____Pe___Ret_deref_check_with_str___Pb____Pb__CUs__Pe____Pe___Type_Arg((((__Pb____Pb__CUs__Pe____Pe___libcall_ret_42 = __ctype_b_loc() , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_42)),((unsigned long long )__Pb____Pb__CUs__Pe____Pe___libcall_ret_42),((unsigned long )(sizeof(__Pb____Pb__CUs__Pe____Pe___libcall_ret_42))))) , __Pb____Pb__CUs__Pe____Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_42))))))))),((unsigned long )(&( *__Pb____Pb__CUs__Pe____Pe___Ret_deref_check_with_str___Pb____Pb__CUs__Pe____Pe___Type_Arg((((__Pb____Pb__CUs__Pe____Pe___libcall_ret_42 = __ctype_b_loc() , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_42)),((unsigned long long )__Pb____Pb__CUs__Pe____Pe___libcall_ret_42),((unsigned long )(sizeof(__Pb____Pb__CUs__Pe____Pe___libcall_ret_42))))) , __Pb____Pb__CUs__Pe____Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_42)))))))[i_index_43]))));
    if (( *__Pb____Pb__CUs__Pe____Pe___Ret_deref_check_with_str___Pb____Pb__CUs__Pe____Pe___Type_Arg((((__Pb____Pb__CUs__Pe____Pe___libcall_ret_42 = __ctype_b_loc() , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_42)),((unsigned long long )__Pb____Pb__CUs__Pe____Pe___libcall_ret_42),((unsigned long )(sizeof(__Pb____Pb__CUs__Pe____Pe___libcall_ret_42))))) , __Pb____Pb__CUs__Pe____Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_42)))))))[i_index_43] & ((unsigned short )((2 < 8?1 << 2 << 8 : 1 << 2 >> 8)))) {
      c = (toupper(c) - 65);
      int keystream = ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , next_keystream_value(deck)) , UL_Ret_pop_from_stack_i_Arg(1));
      int i_index_44;
      (i_index_44 = j , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&encrypted)),((unsigned long )(&encrypted[i_index_44]))));
      encrypted[i_index_44] = ((((int )c) + keystream) % 26 + 65);
//printf("%d\n", keystream);
      j++;
    }
    i++;
  }
  int i_index_45;
  (i_index_45 = j , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&encrypted)),((unsigned long )(&encrypted[i_index_45]))));
  encrypted[i_index_45] = '\0';
  return (v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&encrypted))) , encrypted);
}

char *decrypt(Card *deck,char *message,char *decrypted)
{
//message index
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&decrypted)),UL_Ret_get_from_stack_i_Arg(2));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&message)),UL_Ret_get_from_stack_i_Arg(1));
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )(&deck)),UL_Ret_get_from_stack_i_Arg(0));
  int i = 0;
//decrypted index
  int j = 0;
  char c;
  while(1){
    int i_index_46;
    (i_index_46 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&message)),((unsigned long )(&message[i_index_46]))));
    if (!(message[i_index_46] != '\0' && i < 20)) 
      break; 
    int i_index_47;
    (i_index_47 = i , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&message)),((unsigned long )(&message[i_index_47]))));
    c = message[i_index_47];
    const unsigned short **__Pb____Pb__CUs__Pe____Pe___libcall_ret_48;
    int i_index_49;
    (i_index_49 = ((int )c) , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&( *__Pb____Pb__CUs__Pe____Pe___Ret_deref_check_with_str___Pb____Pb__CUs__Pe____Pe___Type_Arg((((__Pb____Pb__CUs__Pe____Pe___libcall_ret_48 = __ctype_b_loc() , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_48)),((unsigned long long )__Pb____Pb__CUs__Pe____Pe___libcall_ret_48),((unsigned long )(sizeof(__Pb____Pb__CUs__Pe____Pe___libcall_ret_48))))) , __Pb____Pb__CUs__Pe____Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_48))))))))),((unsigned long )(&( *__Pb____Pb__CUs__Pe____Pe___Ret_deref_check_with_str___Pb____Pb__CUs__Pe____Pe___Type_Arg((((__Pb____Pb__CUs__Pe____Pe___libcall_ret_48 = __ctype_b_loc() , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_48)),((unsigned long long )__Pb____Pb__CUs__Pe____Pe___libcall_ret_48),((unsigned long )(sizeof(__Pb____Pb__CUs__Pe____Pe___libcall_ret_48))))) , __Pb____Pb__CUs__Pe____Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_48)))))))[i_index_49]))));
    if (( *__Pb____Pb__CUs__Pe____Pe___Ret_deref_check_with_str___Pb____Pb__CUs__Pe____Pe___Type_Arg((((__Pb____Pb__CUs__Pe____Pe___libcall_ret_48 = __ctype_b_loc() , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_48)),((unsigned long long )__Pb____Pb__CUs__Pe____Pe___libcall_ret_48),((unsigned long )(sizeof(__Pb____Pb__CUs__Pe____Pe___libcall_ret_48))))) , __Pb____Pb__CUs__Pe____Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb____Pb__CUs__Pe____Pe___libcall_ret_48)))))))[i_index_49] & ((unsigned short )((2 < 8?1 << 2 << 8 : 1 << 2 >> 8)))) {
      c = (toupper(c) - 65);
      int keystream = ((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , next_keystream_value(deck)) , UL_Ret_pop_from_stack_i_Arg(1));
      int i_index_50;
      (i_index_50 = j , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&decrypted)),((unsigned long )(&decrypted[i_index_50]))));
      decrypted[i_index_50] = ((((int )c) + (26 - keystream % 26)) % 26 + 65);
//printf("%d\n", keystream);
      j++;
    }
    i++;
  }
  int i_index_51;
  (i_index_51 = j , v_Ret_array_bound_check_using_lookup_UL_Arg_Ul_Arg(((unsigned long long )(&decrypted)),((unsigned long )(&decrypted[i_index_51]))));
  decrypted[i_index_51] = '\0';
  return (v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&decrypted))) , decrypted);
}
static char* global_var52_0_0 = "This software (or technical data) was produced for the U. S.    Government under contract 2009-0917826-016 and is subject to    the Rights in Data-General Clause 52.227-14. Alt. IV (DEC 2007).    (c) Copyright 2012 The MITRE Corporation. All Rights Reserved.";
static char* global_var61_323_18 = "key";
static char* global_var63_323_25 = "r";

struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___Type 
{
  FILE *ptr;
  unsigned long long addr;
}
;

static struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___Type L56R_Ret_create_struct_from_addr_UL_Arg(unsigned long long input1)
{
  struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___Type output;
  output . ptr =  *((FILE **)input1);
  output . addr = input1;
  return output;
}

static struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___Type L56R_Ret_create_struct___Pb__FILE_IO_FILE__typedef_declaration__Pe___Arg_UL_Arg(FILE *input1,unsigned long long input2)
{
  struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___Type output;
  output . ptr = input1;
  output . addr = input2;
  return output;
}

static struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___Type L56R_Ret_assign_and_copy___Pb____Pb__FILE_IO_FILE__typedef_declaration__Pe____Pe___Arg_L56R_Arg(FILE **input1,struct __Pb__FILE_IO_FILE__typedef_declaration__Pe___Type input2)
{
   *input1 = input2 . ptr;
  v_Ret_create_entry_if_src_exists_UL_Arg_UL_Arg(((unsigned long long )input1),((unsigned long long )input2 . addr));
  return L56R_Ret_create_struct___Pb__FILE_IO_FILE__typedef_declaration__Pe___Arg_UL_Arg(input2 . ptr,((unsigned long long )input1));
}
static char* global_var66_325_10 = "Unable to open key file\n";
static char* global_var69_329_10 = "Reading from key file failed\n";

static struct __Pb__v__Pe___Type __Pb__v__Pe___Type_Ret_Cast__Ab_Cardc__typedef_declaration_index_54_Ae__Arg_UL_Arg(Card input1[54],unsigned long long input2)
{
  struct __Pb__v__Pe___Type output;
  output . ptr = ((void *)input1);
  output . addr = input2;
  return output;
}
static char* global_var72_347_11 = "Enter message to be encrypted (type '/help' for help):\n";
static char* global_var74_349_11 = "Enter message to be decrypted (type '/help' for help):\n";
static char* global_var83_373_27 = "/quit";
static char* global_var85_375_34 = "/encrypt";
static char* global_var87_377_11 = "Encryption mode\n";
static char* global_var89_378_34 = "/decrypt";
static char* global_var91_380_11 = "Decryption mode\n";
static char* global_var93_381_34 = "/help";
static char* global_var95_382_11 = "/encrypt: switch to encryption mode\n/decrypt: switch to decryption mode\n/quit: quit program\n";
static char* global_var97_388_12 = "Unencrypted: ";
static char* global_var99_392_13 = "%02X ";
static char* global_var101_401_12 = "\n";
static char* global_var103_402_12 = "Encrypted: %s\n";
static char* global_var107_404_12 = "Encrypted: %s\n";
static char* global_var109_405_12 = "Decrypted: %s\n";

int main(int argc,char *argv[])
{
//char array, not a string
  v_Ret_execAtFirst();
  v_Ret_create_dummy_entry_UL_Arg(((unsigned long long )(&argv)));
  int index_argc;
  for (index_argc = 0; index_argc < argc; ++index_argc) 
    v_Ret_create_dummy_entry_UL_Arg(((unsigned long long )(&argv[index_argc])));
  __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Type_Arg(&data_rights_legend,__Pb__c__Pe___Type_Ret_Cast___Pb__c__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((char *)global_var52_0_0),((unsigned long long )(&global_var52_0_0)),((unsigned long )(sizeof(global_var52_0_0))),((unsigned long long )50)));
  char key[25];
  Card deck[54];
  char out_message[20];
  Card starting_deck[54];
  FILE *keyfile;
//encrypt = 0, decrypt = 1
  int k;
  int mode = 0;
  char premsg[20];
  char in_message[20];
  char pstmsg[20];
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&premsg)),((unsigned long long )premsg),((unsigned long )(sizeof(premsg))),((unsigned long long )50));
  void *__Pb__v__Pe___libcall_ret_54;
  ((__Pb__v__Pe___libcall_ret_54 = memset(__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast__Ab_c_index_20_Ae__Arg_UL_Arg(premsg,((unsigned long long )(&premsg)))),65,sizeof(premsg)) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_54)),((unsigned long long )__Pb__v__Pe___libcall_ret_54),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_54))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_54))));
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&pstmsg)),((unsigned long long )pstmsg),((unsigned long )(sizeof(pstmsg))),((unsigned long long )50));
  void *__Pb__v__Pe___libcall_ret_55;
  ((__Pb__v__Pe___libcall_ret_55 = memset(__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast__Ab_c_index_20_Ae__Arg_UL_Arg(pstmsg,((unsigned long long )(&pstmsg)))),65,sizeof(premsg)) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_55)),((unsigned long long )__Pb__v__Pe___libcall_ret_55),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_55))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_55))));
  int i_index_56;
  (i_index_56 = 0 , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(premsg) / sizeof(char ),((unsigned int )i_index_56)));
  premsg[i_index_56] = 0;
  unsigned int Ui_index_57;
  (Ui_index_57 = sizeof(premsg) - 1 , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(premsg) / sizeof(char ),((unsigned int )Ui_index_57)));
  premsg[Ui_index_57] = 0;
  int i_index_58;
  (i_index_58 = 0 , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(pstmsg) / sizeof(char ),((unsigned int )i_index_58)));
  pstmsg[i_index_58] = 0;
  unsigned int Ui_index_59;
  (Ui_index_59 = sizeof(pstmsg) - 1 , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(pstmsg) / sizeof(char ),((unsigned int )Ui_index_59)));
  pstmsg[Ui_index_59] = 0;
  int x = 20;
//STONESOUP:DATA_TYPE:ARRAY_LENGTH_VARIABLE
  char inputStorage[20];
  int w;
  int y[20];
  for (w = 0; w < 20; ++w) {
    int i_index_60;
    (i_index_60 = w , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(y) / sizeof(int ),((unsigned int )i_index_60)));
    y[i_index_60] = w;
  }
  FILE *__Pb__FILE_IO_FILE__typedef_declaration__Pe___libcall_ret_65;
  L56R_Ret_assign_and_copy___Pb____Pb__FILE_IO_FILE__typedef_declaration__Pe____Pe___Arg_L56R_Arg(&keyfile,(((__Pb__FILE_IO_FILE__typedef_declaration__Pe___libcall_ret_65 = fopen(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var61_323_18),((unsigned long long )(&global_var61_323_18)),((unsigned long )(sizeof(global_var61_323_18))),((unsigned long long )50))),__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var63_323_25),((unsigned long long )(&global_var63_323_25)),((unsigned long )(sizeof(global_var63_323_25))),((unsigned long long )50)))) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__FILE_IO_FILE__typedef_declaration__Pe___libcall_ret_65)),((unsigned long long )__Pb__FILE_IO_FILE__typedef_declaration__Pe___libcall_ret_65),((unsigned long )(sizeof(__Pb__FILE_IO_FILE__typedef_declaration__Pe___libcall_ret_65))))) , L56R_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__FILE_IO_FILE__typedef_declaration__Pe___libcall_ret_65))))));
  if (!keyfile) {
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var66_325_10),((unsigned long long )(&global_var66_325_10)),((unsigned long )(sizeof(global_var66_325_10))),((unsigned long long )50))));
    exit(1);
  }
  char *__Pb__c__Pe___libcall_ret_68;
  if (!(((__Pb__c__Pe___libcall_ret_68 = fgets(key,25,keyfile) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__c__Pe___libcall_ret_68)),((unsigned long long )__Pb__c__Pe___libcall_ret_68),((unsigned long )(sizeof(__Pb__c__Pe___libcall_ret_68))))) , __Pb__c__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__c__Pe___libcall_ret_68))))).addr) {
    printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var69_329_10),((unsigned long long )(&global_var69_329_10)),((unsigned long )(sizeof(global_var69_329_10))),((unsigned long long )50))));
    fclose(keyfile);
    exit(1);
  }
  fclose(keyfile);
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&starting_deck)),((unsigned long long )starting_deck),((unsigned long )(sizeof(starting_deck))),((unsigned long long )50));
  v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&key)),((unsigned long long )key),((unsigned long )(sizeof(key))),((unsigned long long )50));
  (((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&starting_deck))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&key)))) , init_deck(starting_deck,key)) , UL_Ret_pop_from_stack_i_Arg(2));
  k = 0;
  do {
    k++;
    if (k > 20) {
/* Should not have done this many loops, there is a problem */
      return 1;
    }
//set the deck to the key starting position
    v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&deck)),((unsigned long long )deck),((unsigned long )(sizeof(deck))),((unsigned long long )50));
    v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&starting_deck)),((unsigned long long )starting_deck),((unsigned long )(sizeof(starting_deck))),((unsigned long long )50));
    void *__Pb__v__Pe___libcall_ret_71;
    ((__Pb__v__Pe___libcall_ret_71 = memcpy(__Pb__v__Pe___Ret_return_pointer___Pb__v__Pe___Type_Arg(__Pb__v__Pe___Type_Ret_Cast__Ab_Cardc__typedef_declaration_index_54_Ae__Arg_UL_Arg(deck,((unsigned long long )(&deck)))),__Pb__Cv__Pe___Ret_return_pointer___Pb__Cv__Pe___Type_Arg(__Pb__Cv__Pe___Type_Ret_Cast__Ab_Cardc__typedef_declaration_index_54_Ae__Arg_UL_Arg(starting_deck,((unsigned long long )(&starting_deck)))),sizeof(Card ) * 54) , v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_71)),((unsigned long long )__Pb__v__Pe___libcall_ret_71),((unsigned long )(sizeof(__Pb__v__Pe___libcall_ret_71))))) , __Pb__v__Pe___Type_Ret_create_struct_from_addr_UL_Arg(((unsigned long long )(&__Pb__v__Pe___libcall_ret_71))));
//print_deck(deck);
    if (!mode) {
      printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var72_347_11),((unsigned long long )(&global_var72_347_11)),((unsigned long )(sizeof(global_var72_347_11))),((unsigned long long )50))));
    }
    else {
      printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var74_349_11),((unsigned long long )(&global_var74_349_11)),((unsigned long )(sizeof(global_var74_349_11))),((unsigned long long )50))));
    }
    fflush(stdout);
    int i;
    int c = 0;
//STONESOUP:INTERACTION_POINT	//STONESOUP:SOURCE_TAINT:STDIN
    for (i = 0; (c = getchar()) > 0 && c < 256; i++) {
      if (i >= 20) {
        i--;
        break; 
      }
//STONESOUP:DATA_FLOW:ARRAY_INDEX_ARRAY_CONTENT_VALUE
      int i_index_76;
      (i_index_76 = i , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(y) / sizeof(int ),((unsigned int )i_index_76)));
      int i_index_77;
      (i_index_77 = y[i_index_76] , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(inputStorage) / sizeof(char ),((unsigned int )i_index_77)));
      inputStorage[i_index_77] = c;
      int i_index_78;
      (i_index_78 = i , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(inputStorage) / sizeof(char ),((unsigned int )i_index_78)));
      int i_index_79;
      (i_index_79 = i , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(inputStorage) / sizeof(char ),((unsigned int )i_index_79)));
      if (inputStorage[i_index_78] == 10 || inputStorage[i_index_79] == 13) {
        int i_index_80;
        (i_index_80 = i , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(inputStorage) / sizeof(char ),((unsigned int )i_index_80)));
        inputStorage[i_index_80] = '\0';
        break; 
      }
//STONESOUP:CROSSOVER_POINT
    }
    fflush(stdout);
    int msglen = i;
    for (i = 0; i < 20; ++i) {
      int i_index_81;
      (i_index_81 = i , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(in_message) / sizeof(char ),((unsigned int )i_index_81)));
      int i_index_82;
      (i_index_82 = i , v_Ret_array_bound_check_Ui_Arg_Ui_Arg(sizeof(inputStorage) / sizeof(char ),((unsigned int )i_index_82)));
      in_message[i_index_81] = inputStorage[i_index_82];
    }
    v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&in_message)),((unsigned long long )in_message),((unsigned long )(sizeof(in_message))),((unsigned long long )50));
    if (!strcmp(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast__Ab_c_index_20_Ae__Arg_UL_Arg(in_message,((unsigned long long )(&in_message)))),__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var83_373_27),((unsigned long long )(&global_var83_373_27)),((unsigned long )(sizeof(global_var83_373_27))),((unsigned long long )50))))) {
      break; 
    }
    else {
      v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&in_message)),((unsigned long long )in_message),((unsigned long )(sizeof(in_message))),((unsigned long long )50));
      if (!strcmp(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast__Ab_c_index_20_Ae__Arg_UL_Arg(in_message,((unsigned long long )(&in_message)))),__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var85_375_34),((unsigned long long )(&global_var85_375_34)),((unsigned long )(sizeof(global_var85_375_34))),((unsigned long long )50))))) {
        mode = 0;
        printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var87_377_11),((unsigned long long )(&global_var87_377_11)),((unsigned long )(sizeof(global_var87_377_11))),((unsigned long long )50))));
      }
      else {
        v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&in_message)),((unsigned long long )in_message),((unsigned long )(sizeof(in_message))),((unsigned long long )50));
        if (!strcmp(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast__Ab_c_index_20_Ae__Arg_UL_Arg(in_message,((unsigned long long )(&in_message)))),__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var89_378_34),((unsigned long long )(&global_var89_378_34)),((unsigned long )(sizeof(global_var89_378_34))),((unsigned long long )50))))) {
          mode = 1;
          printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var91_380_11),((unsigned long long )(&global_var91_380_11)),((unsigned long )(sizeof(global_var91_380_11))),((unsigned long long )50))));
        }
        else {
          v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&in_message)),((unsigned long long )in_message),((unsigned long )(sizeof(in_message))),((unsigned long long )50));
          if (!strcmp(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast__Ab_c_index_20_Ae__Arg_UL_Arg(in_message,((unsigned long long )(&in_message)))),__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var93_381_34),((unsigned long long )(&global_var93_381_34)),((unsigned long )(sizeof(global_var93_381_34))),((unsigned long long )50))))) {
            printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var95_382_11),((unsigned long long )(&global_var95_382_11)),((unsigned long )(sizeof(global_var95_382_11))),((unsigned long long )50))));
          }
          else {
            if (!mode) {
              char *s;
              v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&in_message)),((unsigned long long )in_message),((unsigned long )(sizeof(in_message))),((unsigned long long )50));
              __Pb__c__Pe___Type_Ret_assign_and_copy___Pb____Pb__c__Pe____Pe___Arg___Pb__c__Pe___Arg_UL_Arg(&s,((char *)in_message),((unsigned long long )(&in_message)));
              printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var97_388_12),((unsigned long long )(&global_var97_388_12)),((unsigned long )(sizeof(global_var97_388_12))),((unsigned long long )50))));
              int i = 0;
              while(1){
                if (!( *__Pb__c__Pe___Ret_deref_check___Pb__c__Pe___Arg_UL_Arg(s,((unsigned long long )(&s))))) 
                  break; 
//STONESOUP:TRIGGER_POINT
                printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var99_392_13),((unsigned long long )(&global_var99_392_13)),((unsigned long )(sizeof(global_var99_392_13))),((unsigned long long )50))),((unsigned int )( *__Pb__c__Pe___Ret_deref_check_with_str___Pb__c__Pe___Type_Arg(__Pb__c__Pe___Type_Ret_Increment___Pb____Pb__c__Pe____Pe___Arg(&s)))) & 0xFF);
/* Keep the printout manageable */
                if (i > msglen + 4) {
                  break; 
                }
                i++;
              }
              printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var101_401_12),((unsigned long long )(&global_var101_401_12)),((unsigned long )(sizeof(global_var101_401_12))),((unsigned long long )50))));
              v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&deck)),((unsigned long long )deck),((unsigned long )(sizeof(deck))),((unsigned long long )50));
              v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&in_message)),((unsigned long long )in_message),((unsigned long )(sizeof(in_message))),((unsigned long long )50));
              v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&out_message)),((unsigned long long )out_message),((unsigned long )(sizeof(out_message))),((unsigned long long )50));
              char *retvar_105;
              unsigned long long popvar_106;
              printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var103_402_12),((unsigned long long )(&global_var103_402_12)),((unsigned long )(sizeof(global_var103_402_12))),((unsigned long long )50))),__Pb__c__Pe___Ret_return_pointer___Pb__c__Pe___Type_Arg(((((((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&in_message)))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&out_message)))) , retvar_105 = encrypt(deck,in_message,out_message)) , popvar_106 = UL_Ret_pop_from_stack_i_Arg(4)) , __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(retvar_105,popvar_106)))));
            }
            else {
              printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var107_404_12),((unsigned long long )(&global_var107_404_12)),((unsigned long )(sizeof(global_var107_404_12))),((unsigned long long )50))),in_message);
              v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&deck)),((unsigned long long )deck),((unsigned long )(sizeof(deck))),((unsigned long long )50));
              v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&in_message)),((unsigned long long )in_message),((unsigned long )(sizeof(in_message))),((unsigned long long )50));
              v_Ret_create_entry_UL_Arg_UL_Arg_Ul_Arg_UL_Arg(((unsigned long long )(&out_message)),((unsigned long long )out_message),((unsigned long )(sizeof(out_message))),((unsigned long long )50));
              char *retvar_111;
              unsigned long long popvar_112;
              printf(__Pb__Cc__Pe___Ret_return_pointer___Pb__Cc__Pe___Type_Arg(__Pb__Cc__Pe___Type_Ret_Cast___Pb__Cc__Pe___Arg_UL_Arg_Ul_Arg_UL_Arg(((const char *)global_var109_405_12),((unsigned long long )(&global_var109_405_12)),((unsigned long )(sizeof(global_var109_405_12))),((unsigned long long )50))),__Pb__c__Pe___Ret_return_pointer___Pb__c__Pe___Type_Arg(((((((v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&deck))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&in_message)))) , v_Ret_push_to_stack_UL_Arg(((unsigned long long )(&out_message)))) , retvar_111 = decrypt(deck,in_message,out_message)) , popvar_112 = UL_Ret_pop_from_stack_i_Arg(4)) , __Pb__c__Pe___Type_Ret_create_struct___Pb__c__Pe___Arg_UL_Arg(retvar_111,popvar_112)))));
            }
/*for(i=0; 1==1; ++i){
				printf("%i - %c\n", in_message[i], in_message[i]);
				fflush(stdout);
				if(in_message[i]=='\0')
					break;
			}
			for(i=0; 1==1; ++i){
				printf("%i - %c\n", out_message[i], out_message[i]);
				fflush(stdout);
				if(in_message[i]=='\0')
					break;
			}*/
          }
        }
      }
    }
//STONESOUP:CONTROL_FLOW:END_CONDITION_CONTROLLED_LOOP
  }while (1);
  v_Ret_execAtLast();
  return 0;
}
